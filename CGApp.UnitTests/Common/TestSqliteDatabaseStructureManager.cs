﻿using CGApp.Data;
using System;
using System.IO;

namespace CGApp.UnitTests
{
    public class TestSqliteDatabaseStructureManager : IDatabaseStructureManager
    {
        private CGAppDbContext DbContext;

        public TestSqliteDatabaseStructureManager(CGAppDbContext dbContext)
        {
            DbContext = dbContext;
        }
        public void ForceToCreateNewDatabase(string fileDbName)
        {
            var fileNamePath = AppContext.BaseDirectory + "\\" + fileDbName;
            if (File.Exists(fileNamePath))
            {
                File.Delete(fileNamePath);
            }

            DbContext.Database.EnsureCreated();
        }
    }
}
