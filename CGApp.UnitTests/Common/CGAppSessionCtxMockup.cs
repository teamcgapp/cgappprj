﻿using System;
using CGApp.WebAPI;

namespace CGApp.UnitTests
{
    public class CGAppSessionCtxMockup : ICGAppSessionCtx
    {
        public CGAppSessionCtxMockup()
        {
            
        }

        public int UserId { get; set; }
        public int CongregationId { get; set; }

        public void Invalidate()
        {
            UserId = 0;
            CongregationId = 0;
        }
    }
}
