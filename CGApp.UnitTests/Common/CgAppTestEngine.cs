﻿using System;
using System.Reflection;
using CGApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using CGApp.WebAPI.Controllers;
using AutoMapper;
using CGApp.WebAPI;

namespace CGApp.UnitTests
{
    public class CgAppTestEngine
    {        
        public void ExecuteTest(
            Action<IServiceProvider> execTest,
            Action<IServiceCollection> configureServiceCollection = null
            )
        {
            const string fileDbName = "sqllite_test.db";

            var serviceCollection = new ServiceCollection();

            serviceCollection
           .AddLogging()
           .AddAutoMapper()
           .AddTransient<IMockupDataGenerator,MockupDataGenerator>()
           .AddSingleton<ICGAppSessionCtx, CGAppSessionCtxMockup>()           
           .AddTransient<IDatabaseStructureManager, TestSqliteDatabaseStructureManager>()
           .AddDbContext<CGAppDbContext>((conf) =>
           {
               conf.UseSqlite($"Data Source={fileDbName}");
           });

           var mvcCoreBuilder = serviceCollection.AddMvcCore();

            mvcCoreBuilder
                .AddApplicationPart(typeof(AuthorizationController).GetTypeInfo().Assembly)
                .AddControllersAsServices();


            if (configureServiceCollection != null)
            {
                configureServiceCollection(serviceCollection);
            }

           var serviceProvider = serviceCollection.BuildServiceProvider();

            var dbStrMgr = serviceProvider.GetService<IDatabaseStructureManager>();
            dbStrMgr.ForceToCreateNewDatabase(fileDbName);


            

            var mdGenerator = serviceProvider.GetService<IMockupDataGenerator>();
            mdGenerator.Generate();

            
            execTest(serviceProvider);


            var dbContext = serviceProvider.GetService<CGAppDbContext>();            

        }
    }
}
