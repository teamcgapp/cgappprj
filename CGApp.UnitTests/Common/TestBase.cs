﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace CGApp.UnitTests
{
    public class TestBase
    {
        protected void ExecuteTest(Action<IServiceProvider> execTest,
                                   Action<IServiceCollection> configureServiceCollection = null)
        {
            var te = new CgAppTestEngine();
            te.ExecuteTest(execTest, configureServiceCollection);

            
        }
    }
}
