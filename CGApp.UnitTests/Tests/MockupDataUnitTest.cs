﻿using CGApp.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
namespace CGApp.UnitTests
{
    [TestClass]
    public class MockupDataUnitTest : TestBase
    {
        [TestMethod]
        public void JustRunTest()
        {
            ExecuteTest(
                (serviceProvider) =>
                {
                    using (var dbCtx = serviceProvider.GetService<CGAppDbContext>())
                    {

                        var lst = (from t in dbCtx.Teritories
                                   select t).ToList();

                        Assert.IsTrue(lst.Count > 0);
                    }
                });
        }


        [TestMethod]
        public void AddingToLoginHistory()
        {
            ExecuteTest(
                (serviceProvider) =>
                {
                    {
                        var dbContext = serviceProvider.GetService<CGAppDbContext>();


                        var lstTheritories =
                                  (from t in dbContext.Teritories
                                   select t).ToList();

                        Assert.IsTrue(lstTheritories.Count > 0);


                        var lstPublishers = (from t in dbContext.Publishers
                                             select t).ToList();

                        Assert.IsTrue(lstPublishers.Count > 0);

                        dbContext.Add(new LoginHistory() { Login = "+XYZ" });
                        dbContext.SaveChanges();



                        var lstLoginHistories = (from t in dbContext.LoginHistories
                                                 select t).ToList();

                        Assert.IsTrue(lstLoginHistories.Count == 1);
                    }

                    {
                        var dbContext = serviceProvider.GetService<CGAppDbContext>();
                        var lstLoginHistories = (from t in dbContext.LoginHistories
                                                 select t).ToList();
                        Assert.IsTrue(lstLoginHistories.Count == 1);
                    }

                });

        }        
    }
}
