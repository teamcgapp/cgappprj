using System;
using System.Reflection;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CGApp.WebAPI;
using CGApp.WebAPI.RemoteModel;
using Microsoft.Extensions.Configuration;
using CGApp.Data;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using CGApp.UnitTests;

namespace CGApp.UnitTests.Tests.Controllers
{
    [TestClass]
    public class PublisherControllerUnitTest:TestBase
    {
        [TestMethod]
        public void Test_Get()
        {
            ExecuteTest((serviceTest) =>
            {
                var sessionCtx = serviceTest.GetService<ICGAppSessionCtx>();
                sessionCtx.CongregationId = 1;

                var ctrl = serviceTest.GetService<PublisherController>();
                var itemList = ctrl.Get().ToList();
                Assert.IsTrue(itemList.Count > 0);
            });
        }

        [TestMethod]
        public void Test_PostAndGet()
        {
            
            /*
            var cnrl = new PublisherController();
            var retPublisher = cnrl.Post(new RMPublisher()
            {
                Name = "Test 1 - name",
                Surname = "Test 1 - surname"
            });

            var retPublisher2 = cnrl.Get(retPublisher.Id);

            Assert.AreEqual(retPublisher.Name, retPublisher2.Name);*/


        }
    }


}
