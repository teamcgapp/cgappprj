using System;
using System.Reflection;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CGApp.WebAPI;
using CGApp.WebAPI.RemoteModel;
using Microsoft.Extensions.Configuration;
using CGApp.Data;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using CGApp.UnitTests;

namespace CGApp.UnitTests.Tests.Controllers
{
    [TestClass]
    public class PublisherGroupControllerUnitTest:TestBase
    {
        [TestMethod]
        public void Test_Get()
        {
            ExecuteTest((serviceTest) =>
            {
                var sessionCtx = serviceTest.GetService<ICGAppSessionCtx>();
                sessionCtx.CongregationId = 1;

                var ctrl = serviceTest.GetService<PublisherGroupController>();
                var itemList = ctrl.Get().ToList();
                Assert.IsTrue(itemList.Count == 5);
            });
        }
       
    }


}
