﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CGApp.WebAPI;
using Microsoft.Extensions.DependencyInjection;
using CGApp.WebAPI.Controllers;

namespace CGApp.UnitTests.Tests.Controllers
{
    [TestClass]
    public class TeritoryControllerUnitTest : TestBase
    {
        [TestMethod]
        public void Test_Get()
        {
            ExecuteTest((serviceTest) =>
            {
                var sessionCtx = serviceTest.GetService<ICGAppSessionCtx>();
                sessionCtx.CongregationId = 1;
                var ctrl = serviceTest.GetService<TeritoryController>();
                var itemList = ctrl.Get().ToList();

                Assert.IsTrue(itemList.Count == 150,"Shuld be 300 teritories");
            });
        }
       
    }


}
