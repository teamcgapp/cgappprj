using System;
using System.Reflection;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CGApp.WebAPI;
using CGApp.WebAPI.RemoteModel;
using Microsoft.Extensions.Configuration;
using CGApp.Data;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using CGApp.UnitTests;
using CGApp.WebAPI.Controllers;
using System.Collections.Generic;

namespace CGApp.UnitTests.Tests.Controllers
{
    [TestClass]
    public class AuthorizationControllerUnitTest : TestBase
    {

        [TestMethod]
        public void AuthorizeOk_1()
        {
            ExecuteTest((serviceProvider) =>
            {
                PerformAuthorizationTest(serviceProvider, "kszybki", "abc", (ctx) =>
                {                    
                    var aa = new AuthorizationAssert(ctx);
                    aa
                    .AuthorizationStatus_ShouldBePositive()
                    .LoginHistory_ShouldBeNotNull()
                    .LoginHistory_IsOk_ShouldBeTrue()
                    .LoginHistory_UserId_ShouldBeEqual(1)
                    .LoginHistory_CongregationId_ShouldBeEqual(1);

                });
            });
        }

        [TestMethod]
        public void AuthorizeNoOk_WrongPassword()
        {
            ExecuteTest((serviceProvider) =>
            {
                PerformAuthorizationTest(serviceProvider, "kszybki", "abc-bad pswd", (ctx) =>
                {
                    var aa = new AuthorizationAssert(ctx);
                    aa
                    .AuthorizationStatus_ShouldBeNegative()
                    .LoginHistory_ShouldBeNotNull()
                    .LoginHistory_IsOk_ShouldBeFalse()
                    .LoginHistory_PasswordWasWrong_ShouldBeTrue()
                    .LoginHistory_UserId_ShouldBeEqual_Zero()
                    .LoginHistory_CongregationId_ShouldBeEqual_Zero();                    
                });
            });
        }

        

        [TestMethod]
        public void AuthorizeNoOk_LoginWasNotFound()
        {
            ExecuteTest((serviceProvider) =>
            {
                PerformAuthorizationTest(serviceProvider, "abc!!!@@", "abc-bad pswd", (ctx) =>
                {
                    var aa = new AuthorizationAssert(ctx);
                    aa
                    .AuthorizationStatus_ShouldBeNegative()
                    .LoginHistory_ShouldBeNotNull()
                    .LoginHistory_IsOk_ShouldBeFalse()
                    .LoginHistory_LoginWasNotFound_ShouldBeTrue()
                    .LoginHistory_UserId_ShouldBeEqual_Zero()
                    .LoginHistory_CongregationId_ShouldBeEqual_Zero();                                                                                                   
                });
            });
        }

        [TestMethod]
        public void AuthorizeNoOk_WrongParameter1()
        {
            ExecuteTest((serviceProvider) =>
            {
                PerformAuthorizationTest(serviceProvider, "", "abc-bad pswd", (ctx) =>
                {
                    var aa = new AuthorizationAssert(ctx);
                    aa
                    .AuthorizationStatus_ShouldBeNegative()
                    .LoginHistory_ShouldBeNotNull()
                    .LoginHistory_IsOk_ShouldBeFalse()
                    .LoginHistory_ParamsWasIncorect_ShouldBeTrue()
                    .LoginHistory_UserId_ShouldBeEqual_Zero()
                    .LoginHistory_CongregationId_ShouldBeEqual_Zero();
                    
                    
                });
            });
        }
        
        #region lib test function for authorization
        public class AuthorizationAssert
        {
            AuthorizationAssertCtx AuthorizationAssertCtx { get; set; }

            public AuthorizationAssert(AuthorizationAssertCtx authorizationAssertCtx)
            {
                AuthorizationAssertCtx = authorizationAssertCtx;
            }
            public AuthorizationAssert AuthorizationStatus_ShouldBePositive()
            {
                Assert.IsTrue(
                    AuthorizationAssertCtx.AuthorizationResult.AuthorizationStatus,
                    $"{nameof(AuthorizationAssertCtx.AuthorizationResult)}.{nameof(AuthorizationAssertCtx.AuthorizationResult.AuthorizationStatus)} [{nameof(AuthorizationStatus_ShouldBePositive)}]");
                return this;
            }

            public AuthorizationAssert AuthorizationStatus_ShouldBeNegative()
            {
                Assert.IsFalse(
                    AuthorizationAssertCtx.AuthorizationResult.AuthorizationStatus,
                    $"{nameof(AuthorizationAssertCtx.AuthorizationResult)}.{nameof(AuthorizationAssertCtx.AuthorizationResult.AuthorizationStatus)} [{nameof(AuthorizationStatus_ShouldBeNegative)}]");
                return this;
            }

            public AuthorizationAssert LoginHistory_ShouldBeNotNull()
            {
                Assert.IsNotNull(
                    AuthorizationAssertCtx.LoginHistory, 
                    $"{nameof(AuthorizationAssertCtx.LoginHistory)} [{nameof(LoginHistory_ShouldBeNotNull)}]");
                return this;
            }

            public AuthorizationAssert LoginHistory_IsOk_ShouldBeTrue()
            {
                Assert.IsTrue(
                    AuthorizationAssertCtx.LoginHistory.IsOk, 
                    $"{nameof(AuthorizationAssertCtx.LoginHistory)}.{nameof(AuthorizationAssertCtx.LoginHistory.IsOk)} [{nameof(LoginHistory_IsOk_ShouldBeTrue)}]");
                return this;
            }

            public AuthorizationAssert LoginHistory_IsOk_ShouldBeFalse()
            {
                Assert.IsFalse(
                    AuthorizationAssertCtx.LoginHistory.IsOk, 
                    $"{nameof(AuthorizationAssertCtx.LoginHistory)}.{nameof(AuthorizationAssertCtx.LoginHistory.IsOk)} [{nameof(LoginHistory_IsOk_ShouldBeFalse)}]");
                return this;
            }

            public AuthorizationAssert LoginHistory_LoginWasNotFound_ShouldBeTrue()
            {
                Assert.IsTrue(
                    Convert.ToBoolean(AuthorizationAssertCtx.LoginHistory.LoginWasNotFound), 
                    $"{nameof(AuthorizationAssertCtx.LoginHistory)}.{nameof(AuthorizationAssertCtx.LoginHistory.LoginWasNotFound)} [{nameof(LoginHistory_LoginWasNotFound_ShouldBeTrue)}]");
                return this;
            }


            public AuthorizationAssert LoginHistory_PasswordWasWrong_ShouldBeTrue()
            {
                Assert.IsTrue(
                    Convert.ToBoolean(AuthorizationAssertCtx.LoginHistory.PasswordWasWrong),
                    $"{nameof(AuthorizationAssertCtx.LoginHistory)}.{nameof(AuthorizationAssertCtx.LoginHistory.PasswordWasWrong)} [{nameof(LoginHistory_PasswordWasWrong_ShouldBeTrue)}]");
                return this;
            }

            public AuthorizationAssert LoginHistory_ParamsWasIncorect_ShouldBeTrue()
            {
                Assert.IsTrue(
                    Convert.ToBoolean(AuthorizationAssertCtx.LoginHistory.ParamsWasIncoret),
                    $"{nameof(AuthorizationAssertCtx.LoginHistory)}.{nameof(AuthorizationAssertCtx.LoginHistory.ParamsWasIncoret)} [{nameof(LoginHistory_ParamsWasIncorect_ShouldBeTrue)}]");
                return this;
            }

            public AuthorizationAssert LoginHistory_UserId_ShouldBeEqual_Zero()
            {
                Assert.AreEqual(
                    AuthorizationAssertCtx.SessionCtx.UserId, 0,
                    $"{nameof(AuthorizationAssertCtx.SessionCtx.CongregationId)} [{nameof(LoginHistory_UserId_ShouldBeEqual_Zero)}]");
                return this;
            }


            public AuthorizationAssert LoginHistory_UserId_ShouldBeEqual(int val)
            {
                Assert.AreEqual(
                    AuthorizationAssertCtx.SessionCtx.UserId, val,
                    $"{nameof(AuthorizationAssertCtx.SessionCtx.CongregationId)} [{nameof(LoginHistory_UserId_ShouldBeEqual)} = {val}]");
                return this;
            }

            public AuthorizationAssert LoginHistory_CongregationId_ShouldBeEqual_Zero()
            {
                Assert.AreEqual(
                    AuthorizationAssertCtx.SessionCtx.CongregationId, 0, 
                    $"{nameof(AuthorizationAssertCtx.SessionCtx.CongregationId)} [{nameof(LoginHistory_CongregationId_ShouldBeEqual_Zero)}]");
                return this;
            }


            public AuthorizationAssert LoginHistory_CongregationId_ShouldBeEqual(int val)
            {
                Assert.AreEqual(
                    AuthorizationAssertCtx.SessionCtx.CongregationId, val,
                    $"{nameof(AuthorizationAssertCtx.SessionCtx.CongregationId)} [{nameof(LoginHistory_CongregationId_ShouldBeEqual)} = {val}]");
                return this;
            }

        }

        
        public class AuthorizationAssertCtx
        {
            public Publisher Publisher { get; set; }
            public LoginHistory LoginHistory { get; set; }

            public ICGAppSessionCtx SessionCtx { get; set; }

            public RMAuthorizationResult AuthorizationResult { get; set; }
        }

        private void PerformAuthorizationTest(IServiceProvider serviceProvider, string login, string password, Action<AuthorizationAssertCtx> assertionF)
        {
            RMAuthorizationResult authorizationResult = null;

            {
                var ctrl = serviceProvider.GetService<AuthorizationController>();
                authorizationResult = ctrl.AutorizeUser(new RMAuthorizationParams() { Login = login, Password = password });

                var dbCtxt = serviceProvider.GetService<CGAppDbContext>();
                var pub = (from p in dbCtxt.Publishers
                           where p.Login == login
                           select p).SingleOrDefault();
                if (authorizationResult.AuthorizationStatus)
                {
                    if (pub != null)
                    {
                        var sessionCtx = serviceProvider.GetService<ICGAppSessionCtx>();
                        Assert.AreEqual(pub.Id, sessionCtx.UserId, "UserId in sessionCtx should be set to the same as publisher");
                        Assert.AreEqual(pub.Congregation.Id, sessionCtx.CongregationId, "CongregationId in sessionCtx should be set to congregation of publisher");
                    }
                }
            }
            {
                var dbCtxt = serviceProvider.GetService<CGAppDbContext>();
                var sessionCtx = serviceProvider.GetService<ICGAppSessionCtx>();
                var pub = (from p in dbCtxt.Publishers
                           where p.Login == login
                           select p).SingleOrDefault();

                List<LoginHistory> loginHistory = new List<LoginHistory>();
                if (pub != null)
                {
                    loginHistory = (from h in dbCtxt.LoginHistories
                                    where h.Publisher.Id == pub.Id
                                    orderby h.Id
                                    select h).ToList();
                }
                else
                {
                    loginHistory = (from h in dbCtxt.LoginHistories
                                    orderby h.Id
                                    select h).ToList();
                }

                var aCtx = new AuthorizationAssertCtx()
                {
                    Publisher = pub,
                    LoginHistory = loginHistory.LastOrDefault(),
                    SessionCtx = sessionCtx,
                    AuthorizationResult = authorizationResult
                };
                assertionF(aCtx);
            }
        }        
        #endregion
    }


}
