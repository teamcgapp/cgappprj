﻿using CGApp.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutoMapper;
using CGApp.WebAPI.RemoteModel;
using Microsoft.EntityFrameworkCore;

namespace CGApp.UnitTests.Tests.Mapping
{
    [TestClass]
    public class MappingUnitTest: TestBase
    {

        [TestMethod]
        public void PublisherToRMPublisher()
        {
            ExecuteTest(
                (serviceProvider) =>
                {
                    var mapper = serviceProvider.GetService<IMapper>();

                    using (var dbCtx = serviceProvider.GetService<CGAppDbContext>())
                    {

                        var lst = (from t in dbCtx.Publishers
                                   select t).ToList();

                        foreach(var item in lst)
                        {
                            RMPublisher rmPublisher = mapper.Map<Publisher, RMPublisher>(item);
                        }                        
                    }
                });

        }


        

        
    }
}
