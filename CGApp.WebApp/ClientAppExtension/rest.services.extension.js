"use strict";
var ClientBase = (function () {
    function ClientBase() {
    }
    ClientBase.prototype.transformOptions = function (options) {
        return Promise.resolve(options);
    };
    ClientBase.prototype.transformResult = function (url, response, processor) {
        return processor(response);
    };
    return ClientBase;
}());
exports.ClientBase = ClientBase;
//# sourceMappingURL=rest.services.extension.js.map