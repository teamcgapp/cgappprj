﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

using NSwag.CodeGeneration;
using NSwag.CodeGeneration.TypeScript;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using AutoMapper;
using CGApp.Data;
using NSwag.AspNetCore;
using NJsonSchema;
using System.Reflection;
using CGApp.WebAPI;

namespace CGApp.WebApp
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();            
            //test

        }

        public ModelExplorer ModelExplorer { get; set; }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var fileDbName = "cgapp_database.db";
            // Add framework services.
            services.AddDistributedMemoryCache();            
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromSeconds(300);
                options.CookieHttpOnly = true;                
            });

            services.AddMvc();

            services.AddTransient<IMockupDataGenerator, MockupDataGenerator>()
                .AddTransient<ICGAppSessionCtx,CGAppSessionCtx>();            

            services           
           .AddAutoMapper()
           .AddDbContext<CGAppDbContext>((conf) =>
           {
               conf.UseSqlite(Configuration["Data:DefaultConnection:ConnectionString"]);
           });


            //services.AddSingleton<ICGAppSessionCtx, CGAppSessionCtx>();
            /*
            // Swagger services.
            services.AddSwaggerGen();
            services.ConfigureSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Version = "v1",
                    Title = "Test API",
                    Description = "Jesse's Test API",
                    TermsOfService = "None",
                    Contact = new Contact() { Name = "JESSE.NET", Email = "info@test.com", Url = "www.JesseDotNet.com" }
                });
                options.IncludeXmlComments(GetSwaggerXMLPath());
                options.DescribeAllEnumsAsStrings();
            });*/

            //var dbFileName = Configuration["Data:DefaultConnection:FileName"];


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app, 
            IHostingEnvironment env, 
            ILoggerFactory loggerFactory, 
            CGAppDbContext dbCtx, 
            IMockupDataGenerator mdGenerator            
            )
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            /*
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }*/

            app.UseDefaultFiles();
            app.UseStaticFiles();

            //app.UseInMemorySession(configure: s => s.IdleTimeout = TimeSpan.FromMinutes(30));
            app.UseSession();
            app.UseMvc();
            
            app.UseSwaggerUi(typeof(PublisherController).GetTypeInfo().Assembly, new SwaggerUiOwinSettings
            {

                DefaultPropertyNameHandling = PropertyNameHandling.CamelCase

            });

            
            dbCtx.Database.EnsureCreated();
            mdGenerator.Generate();
            
        }
    }
}
