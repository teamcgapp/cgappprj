import { NgModule }      from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { CgAppADOServiceMockup } from "./services/cg.app.ado.mockup";


//import {Observable} from "rxjs/Observable";

import {PanelModule} from 'primeng/primeng';
import {DataTableModule,SharedModule} from 'primeng/primeng';
import {MenuModule} from 'primeng/primeng';
import {PasswordModule} from 'primeng/primeng';
import {InputTextModule} from 'primeng/primeng';
import {MessagesModule} from 'primeng/primeng';
import {MenubarModule} from 'primeng/primeng';
import {ToolbarModule} from 'primeng/primeng';
import {FieldsetModule} from 'primeng/primeng';
import {CheckboxModule} from 'primeng/primeng';
import {MultiSelectModule} from 'primeng/primeng';
import {ButtonModule} from 'primeng/primeng';
import {CalendarModule} from "primeng/primeng";


import {MainView} from "./components/main/main-view.component";
import {HomeViewComponent} from "./components/appviews/home/home-view.component";
import {PublisherListComponent} from "./components/appviews/publisher/publisher-list.component";
import {PublisherListFilterComponent} from "./components/appviews/publisher/publisher-list-filter.component";
import {AppComponent}   from './components/main/app.component';
import {PublisherDetailComponent} from "./components/appviews/publisher/publisher-detail.component";
import {UserLoginComponent} from "./components/login/login.component";
import {MainToolbar} from "./components/main/main-toolbar.component";
import {NavigationPanelComponent} from "./components/main/nav-panel.component";
import {PanelConfirmationComponent} from "./components/main/panel-confirmation.component";
import {TerritoryListComponent} from "./components/appviews/teritory/territory-list.component";
import {TerritoryListFilterComponent} from "./components/appviews/teritory/territory-list-filter.component";



export const ROUTES: Routes = [
    {path: 'login', component: UserLoginComponent},
    {path: 'home', component: MainView,data: {panelTitle: 'List of publisher'},
        children:[
            {path: '', component: HomeViewComponent}]
    },
    {path: 'publisher_view', component: MainView ,
        children:[
          {path: '', component: PublisherListComponent, data: {panelTitle: 'List of publisher'}},
          {path: ':id', component: PublisherDetailComponent, data: {panelTitle: 'List of publisher'}}
            //{path: 'publisher_view_filter', component: PublisherListFilterComponent, outlet:'left'}
          //{path: 'publisher_view_filter', component: PublisherListFilterComponent, outlet:'left'}
            ]
    },
    {path: 'territory_view', component: MainView, data: {panelTitle: 'List of teritories'},
        children:[
            {path: '', component: TerritoryListComponent}
        ]
    },
    {path: '', redirectTo:'login' , pathMatch:'prefix'},
    ///{path: '', redirectTo:'publisher_view/(left:publisher_view_filter)' , pathMatch:'prefix'},
    {path: '**', redirectTo:'publisher_view' , pathMatch:'prefix'}

];




@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(ROUTES),

        PanelModule,
        DataTableModule,
        SharedModule,
        MenuModule,

        PasswordModule,
        InputTextModule,
        CalendarModule,
        MessagesModule,

        MenubarModule,
        ToolbarModule,

        ButtonModule,

        FieldsetModule,
        CheckboxModule,
        MultiSelectModule
    ],
    declarations: [ AppComponent,
        HomeViewComponent,
        MainToolbar,
        NavigationPanelComponent,
        MainView,
        PublisherListComponent,PublisherListFilterComponent,PublisherDetailComponent,
        TerritoryListComponent,TerritoryListFilterComponent,
        UserLoginComponent,PanelConfirmationComponent ],
    bootstrap:    [ AppComponent ],
    providers: [{ provide: "ICgAppADOService", useClass: CgAppADOServiceMockup }]
})
export class AppModule { }
