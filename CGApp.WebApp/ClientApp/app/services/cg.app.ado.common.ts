import {Observable} from "rxjs";
import {PublisherModel, PublisherGroupModel, TeritoryModel} from "./cg.app.model";

export interface IPublisherService
{
    getList():Observable<PublisherModel[]>;
}

export interface IPublisherGroupService
{
    getList():Observable<PublisherGroupModel[]>;
}

export interface  IAuthorizationService {
    AutorizeUser(authorizationParams: AuthorizationParams):Observable<AuthorizationResult>;
    Logout():Observable<boolean>;
}


export interface ITeritoryService
{
    getList():Observable<TeritoryModel[]>;
}

export interface ICgAppADOService
{
    AuthorizationState: AuthorizationState;
    Publisher: IPublisherService;
    PublisherGroup: IPublisherGroupService;
    Authorization: IAuthorizationService;
    Theritory: ITeritoryService;
}

export class AuthorizationParams
{
    Login:string;
    Password:string;
}

export class AuthorizationResult
{
    public AuthorizationStatus: boolean;
}


export class AuthorizationState
{
    UserIsAuthorized: boolean;
    Login:string;
    Fullname:string;
}