export class PublisherModel
{
    Id:number;
    Login:string;
    IsDelete:boolean;
    IsOnlyLogin:boolean;
    CanLogin:boolean;
    CanLoginToPublisherPanel:boolean;
    Name: string;
    Surname:string;
    DateOfBirth: Date;
    DateOfBaptism: Date;
    Sex:SexEnum;
    //SexDesc: string;
    Privilage: PrivilageModel;
    PublisherGroup: PublisherGroupModel;

}

export class PrivilageModel
{
    IsNotPublisher: boolean;
    IsUnbaptizedPublishers: boolean;
    IsBaptizedPublishers: boolean;

    IsMinisterialServant:boolean;
    IsElder:boolean;

    IsRegularPioneer:boolean;
    IsAuxiliaryPioneer:boolean;
    IsSpecialPioneer:boolean;

    IsGroupServiceOverseer:boolean;

    IsGroupServiceDeputyOverseer:boolean;

    CanMobileCart: boolean;
}


export class TeritoryModel
{
    Id: number;
    Name:string;
    Streets: Array<TeritoryAdressModel>;
    Description: string;
    PublisherGroup:PublisherGroupModel;
    Publisher: PublisherModel;

}

export class TeritoryAdressModel
{

    Street:string;
    BuidingRange:string;
    ZipCode:string;
}

export class PrivilageModelEx
{
    public static ClearFlags(pivilageModel: PrivilageModel): void
    {
        pivilageModel.IsNotPublisher = false;
        pivilageModel.IsUnbaptizedPublishers = false;
        pivilageModel.IsBaptizedPublishers = false;

        pivilageModel.IsMinisterialServant = false;
        pivilageModel.IsElder = false;

        pivilageModel.IsRegularPioneer = false;
        pivilageModel.IsAuxiliaryPioneer = false;
        pivilageModel.IsSpecialPioneer= false;
        pivilageModel.IsGroupServiceOverseer = false;
        pivilageModel.IsGroupServiceDeputyOverseer = false;
        pivilageModel.CanMobileCart = false;
    }

    public static IsAllUnset(pivilageModel: PrivilageModel): boolean
    {
        return (!pivilageModel.IsNotPublisher) &&
            (!pivilageModel.IsUnbaptizedPublishers) &&
            (!pivilageModel.IsBaptizedPublishers) &&

            (!pivilageModel.IsMinisterialServant) &&
            (!pivilageModel.IsElder) &&

            (!pivilageModel.IsRegularPioneer) &&
            (!pivilageModel.IsAuxiliaryPioneer) &&
            (!pivilageModel.IsSpecialPioneer) &&

            (!pivilageModel.IsGroupServiceOverseer) &&
            (!pivilageModel.IsGroupServiceDeputyOverseer) &&

            (!pivilageModel.CanMobileCart);


    }
}

export class PublisherGroupModel
{
    Id:number;
    Name:string;
    Pos:number;
}

export enum SexEnum {Male, Female};
