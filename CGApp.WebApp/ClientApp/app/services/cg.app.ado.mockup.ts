import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx';
import {Enumerable} from "typescript-dotnet-commonjs/System.Linq/Linq";
import {DateTime} from "typescript-dotnet-commonjs/System/Time/DateTime";
import {PublisherModel, PublisherGroupModel, TeritoryModel, TeritoryAdressModel, SexEnum} from "./cg.app.model";
import {
    IPublisherService, IPublisherGroupService, IAuthorizationService, ICgAppADOService,
    AuthorizationState, AuthorizationParams, AuthorizationResult, ITeritoryService
} from "./cg.app.ado.common";

//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';






export class PublisherServiceMockup implements IPublisherService
{

    public constructor (private _adoService: CgAppADOServiceMockup) {}

    public getList():Observable<PublisherModel[]>
    {

        let source = Observable.create(
            observer => {
                observer.next(this._adoService.Publishers);
                observer.complete();
            }
        );

        return source;
    }
}

export class TeritoryServiceMockup implements  ITeritoryService
{
    public constructor (private _adoService: CgAppADOServiceMockup) {}

    public getList():Observable<TeritoryModel[]>
    {

        let source = Observable.create(
            observer => {
                observer.next(this._adoService.Teritories);
                observer.complete();
            }
        );

        return source;
    }
}



export class PublisherGroupServiceMockup implements IPublisherGroupService
{

    public constructor (private _adoService: CgAppADOServiceMockup) {}

    public getList():Observable<PublisherGroupModel[]>
    {

        let source = Observable.create(
            observer => {
                observer.next(this._adoService.PublisherGroups);
                observer.complete();
            }
        );

        return source;
    }
}



export class AuthorizationServiceMockup implements IAuthorizationService
{
    public constructor (private _adoService: CgAppADOServiceMockup) {}

    public AutorizeUser(authorizationParams: AuthorizationParams):Observable<AuthorizationResult>
    {

            let publisher : PublisherModel = null;


            Observable.from(this._adoService.Publishers).
                filter( (p)=> p.Login == authorizationParams.Login).
                subscribe( (x)=> publisher = x  );


        let res = new AuthorizationResult();
        res.AuthorizationStatus = false;
        if (publisher !== null) {
            if (authorizationParams.Password === 'abc') {
                res.AuthorizationStatus = true;
            }
        }

        if (res.AuthorizationStatus)
        {
            this._adoService.AuthorizationState.UserIsAuthorized = true;
            this._adoService.AuthorizationState.Login = authorizationParams.Login;
        }
        else
        {
            this._adoService.AuthorizationState.UserIsAuthorized = false;
            this._adoService.AuthorizationState.Login = null;
        }



        let source = Observable.create(
            observer => {
                observer.next(res);
                observer.complete();
            }
        );
        return source;
    }

    public Logout():Observable<boolean>
    {
        this._adoService.AuthorizationState.UserIsAuthorized = false;
        this._adoService.AuthorizationState.Login = null;

        let source = Observable.create(
            observer => {
                observer.next(true);
                observer.complete();
            }
        );
        return source;
    }
}


@Injectable()
export class CgAppADOServiceMockup implements ICgAppADOService {
    public Publishers: Array<PublisherModel>;
    public Teritories: Array<TeritoryModel>;
    public PublisherGroups: Array<PublisherGroupModel>;
    public AuthorizationState: AuthorizationState;
    public gr1: PublisherGroupModel;
    public gr2: PublisherGroupModel;
    public gr3: PublisherGroupModel;
    public gr4: PublisherGroupModel;

    private _publisherService: IPublisherService;
    private _teritoryService: ITeritoryService;
    private _publisherGroup: IPublisherGroupService;
    private _authorization: IAuthorizationService;

    public constructor() {
        this.InitializeMockupData();
        this.AuthorizationState = new AuthorizationState();
        this._authorization = new AuthorizationServiceMockup(this);
        this._publisherService = new PublisherServiceMockup(this);
        this._publisherGroup = new PublisherGroupServiceMockup(this);
        this._teritoryService = new TeritoryServiceMockup(this);


        let val:DateTime = new DateTime('dd');



    }

    public get Publisher(): IPublisherService {
        return this._publisherService;
    }

    public get PublisherGroup(): IPublisherGroupService {
        return this._publisherGroup;
    }

    public get Authorization(): IAuthorizationService {
        return this._authorization;
    }

    public get Theritory(): ITeritoryService {
        return this._teritoryService;
    }


    private InitializeMockupData():void
    {

        console.log("ABCD " + DateTime.now.toString());
        this.gr1 = {Id:1,Pos:1,Name:'Grupa 1'};
        this.gr2 = {Id:2,Pos:2,Name:'Grupa 2'};
        this.gr3 = {Id:3,Pos:3,Name:'Grupa 3'};
        this.gr4 = {Id:4,Pos:4,Name:'Grupa 4'};

        this.PublisherGroups = [this.gr1,this.gr2,this.gr3,this.gr4];

        this.generatePublishers();
        this.generateTerytories();
    }

    private generateTerytories():void
    {
        var ters: Array<TeritoryModel> = [];

        var teritoryGenState = new TeritoryGenState();
        teritoryGenState.Id = 0;
        this.addTeritories(ters,teritoryGenState,135);

        this.attachPublisherToTeritories(ters,1,1,25);
        this.attachPublisherToTeritories(ters,2,26,50);
        this.attachPublisherToTeritories(ters,3,51,75);
        this.attachPublisherToTeritories(ters,4,76,100);



        this.Teritories = ters;
    }

    private attachPublisherToTeritories(
        ters:Array<TeritoryModel>,
        pubGroupId: number,
        terIdStart: number,
        terIdEnd:number): void
    {
        let tersAsEnumerable = Enumerable.from(ters);;
        let groupAsEnumerable = Enumerable.from(this.PublisherGroups);
        let pubAsEnumerable = Enumerable.from(this.Publishers);


        var  ters_for_group = tersAsEnumerable.where( (t)=> t.Id >= terIdStart && t.Id <= terIdEnd);
        var pub_group = groupAsEnumerable.where((g)=> g.Id == pubGroupId).singleOrDefault();
        var tersIndex = 0;
        ters_for_group.forEach( (t)=>
        {
            t.PublisherGroup = pub_group;
        });

        var pub_for_group = pubAsEnumerable.where((p)=> p.PublisherGroup.Id == pubGroupId);

        var ters_for_group_array = ters_for_group.toArray();
        pub_for_group.forEach((pub)=>
        {
            ters_for_group_array[tersIndex].Publisher = pub;
            tersIndex++;

        });
    }

    private addTeritories(teritories: Array<TeritoryModel>, teritoryGenState: TeritoryGenState,  count:number):void
    {
        for(var i=0; i<count; i++)
        {
            teritoryGenState.Id = teritoryGenState.Id +1;
            teritories.push(
                {
                    Id: teritoryGenState.Id,
                    Name: 'Teritory - ' + teritoryGenState.Id,
                    Streets: new Array<TeritoryAdressModel>(),
                    Description: '',
                    PublisherGroup: null,
                    Publisher:null
                }
            )
        }

    }
    private generatePublishers():void
    {
        var pubs: Array<PublisherModel> =  [
            {Id: 1, IsDelete:false,CanLogin:true, CanLoginToPublisherPanel:false, IsOnlyLogin :false, Login:'kamil.predki',
                Sex:SexEnum.Male, Name: 'Kamil', Surname: 'Prędki',
                DateOfBirth:new DateTime("1979-10-11").toJsDate(),DateOfBaptism: new DateTime("1991-01-05").toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant:false,
                    IsElder:true,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer:false,
                    IsSpecialPioneer:false,
                    IsGroupServiceOverseer:true,
                    IsGroupServiceDeputyOverseer:false,
                    CanMobileCart:true}},

            {Id: 2, IsDelete:false,CanLogin:false,CanLoginToPublisherPanel:false, IsOnlyLogin:false, Login:'',
                Sex:SexEnum.Female, Name: 'Anna', Surname: 'Prędki',
                DateOfBirth: new DateTime('1981-11-13').toJsDate(),DateOfBaptism: new DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant:false,
                    IsElder:false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer:false,
                    IsSpecialPioneer:false,
                    IsGroupServiceOverseer:false,
                    IsGroupServiceDeputyOverseer:false,
                    CanMobileCart:true}},

            {Id: 3, IsDelete:false,CanLogin:true,CanLoginToPublisherPanel:false, IsOnlyLogin:false, Login:'janusz.predki',
                Sex:SexEnum.Male, Name: 'Janusz', Surname: 'Prędki',
                DateOfBirth: new DateTime('1990-01-15').toJsDate(),DateOfBaptism: new DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: true,
                    IsElder:false,
                    IsRegularPioneer: true,
                    IsAuxiliaryPioneer:false,
                    IsSpecialPioneer:false,
                    IsGroupServiceOverseer:false,
                    IsGroupServiceDeputyOverseer:false,
                    CanMobileCart:true}},

            {Id: 4, IsDelete:false,CanLogin:true,CanLoginToPublisherPanel:false,  IsOnlyLogin:false, Login:'szymon.sprytny',
                Sex:SexEnum.Male, Name: 'Szymon', Surname: 'Sprytny',
                DateOfBirth: new DateTime('1985-04-05').toJsDate(),DateOfBaptism:new DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr2,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant:false,
                    IsElder:true,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer:false,
                    IsSpecialPioneer:false,
                    IsGroupServiceOverseer:true,
                    IsGroupServiceDeputyOverseer:false,
                    CanMobileCart:true}},

            {Id: 5, IsDelete:false,CanLogin:false,CanLoginToPublisherPanel:false, IsOnlyLogin:false, Login:'',
                Sex:SexEnum.Female, Name: 'Katarzyna', Surname: 'Sprytny',
                DateOfBirth: new DateTime('1979-10-11').toJsDate(),DateOfBaptism:new DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr2,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant:false,
                    IsElder:false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer:false,
                    IsSpecialPioneer:false,
                    IsGroupServiceOverseer:false,
                    IsGroupServiceDeputyOverseer:false,
                    CanMobileCart:true}},

            {Id: 6, IsDelete:false,CanLogin:true,CanLoginToPublisherPanel:false, IsOnlyLogin:false, Login:'tomasz.gorliwy',
                Sex:SexEnum.Male, Name: 'Tomasz', Surname: 'Gorliwy',
                DateOfBirth: new DateTime('1979-10-11').toJsDate(),DateOfBaptism: new DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr2,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant:true,
                    IsElder:false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer:false,
                    IsSpecialPioneer:false,
                    IsGroupServiceOverseer:false,
                    IsGroupServiceDeputyOverseer:false,
                    CanMobileCart:true}},

            {Id: 7, IsDelete:false,CanLogin:false,CanLoginToPublisherPanel:false, IsOnlyLogin:false, Login:'',
                Sex:SexEnum.Female, Name: 'Marta', Surname: 'Gorliwy',
                PublisherGroup: this.gr2,
                DateOfBirth: new DateTime('1979-10-11').toJsDate(),DateOfBaptism: new DateTime('1991-01-05').toJsDate(),
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant:false,
                    IsElder:false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer:false,
                    IsSpecialPioneer:false,
                    IsGroupServiceOverseer:false,
                    IsGroupServiceDeputyOverseer:false,
                    CanMobileCart:true}},

            {Id: 8, IsDelete:false,CanLogin:true,CanLoginToPublisherPanel:false, IsOnlyLogin:false, Login:'mateusz.pomyslowy',
                Sex:SexEnum.Male, Name: 'Mateusz', Surname: 'Pomysłowy',
                DateOfBirth: new DateTime('1979-10-11').toJsDate(),DateOfBaptism:new DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr2,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant:true,
                    IsElder:false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer:false,
                    IsSpecialPioneer:false,
                    IsGroupServiceOverseer:false,
                    IsGroupServiceDeputyOverseer:false,
                    CanMobileCart:true}},

            {Id: 9, IsDelete:false,CanLogin:false,CanLoginToPublisherPanel:false, IsOnlyLogin:false, Login:'',
                Sex:SexEnum.Female, Name: 'Joanna', Surname: 'Pomysłowy',
                DateOfBirth: new DateTime('1979-10-11').toJsDate(),DateOfBaptism: new DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr2,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant:false,
                    IsElder:false,
                    IsRegularPioneer: true,
                    IsAuxiliaryPioneer:false,
                    IsSpecialPioneer:false,
                    IsGroupServiceOverseer:false,
                    IsGroupServiceDeputyOverseer:false,
                    CanMobileCart:true}},

            {Id: 10,IsDelete:false,CanLogin:false,CanLoginToPublisherPanel:false, IsOnlyLogin:false, Login:'',
                Sex:SexEnum.Male, Name: 'Dobromir', Surname: 'Pomysłowy',
                DateOfBirth: new DateTime('1979-10-11').toJsDate(),DateOfBaptism: new DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant:false,
                    IsElder:false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer:false,
                    IsSpecialPioneer:false,
                    IsGroupServiceOverseer:false,
                    IsGroupServiceDeputyOverseer:false,
                    CanMobileCart:true}},

            {Id: 11,IsDelete:false,CanLogin:false,CanLoginToPublisherPanel:false, IsOnlyLogin:false, Login:'',
                Sex:SexEnum.Female, Name: 'Katarzyna', Surname: 'Pomysłowy',
                DateOfBirth: new DateTime('1979-10-11').toJsDate(),DateOfBaptism: new DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant:false,
                    IsElder:false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer:true,
                    IsSpecialPioneer:false,
                    IsGroupServiceOverseer:false,
                    IsGroupServiceDeputyOverseer:false,
                    CanMobileCart:true}},

            {Id: 12,IsDelete:false,CanLogin:false,CanLoginToPublisherPanel:false, IsOnlyLogin:false, Login:'',
                Sex:SexEnum.Female, Name: 'Joanna', Surname: 'Późny',
                DateOfBirth: new DateTime('1979-10-11').toJsDate(),DateOfBaptism: new DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant:false,
                    IsElder:false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer:false,
                    IsSpecialPioneer:false,
                    IsGroupServiceOverseer:false,
                    IsGroupServiceDeputyOverseer:false,
                    CanMobileCart:true}},

            {Id: 13,IsDelete:false,CanLogin:false,CanLoginToPublisherPanel:false, IsOnlyLogin:false, Login:'',
                Sex:SexEnum.Male, Name: 'Kamil', Surname: 'Późny',
                DateOfBirth: new DateTime('1979-10-11').toJsDate(),DateOfBaptism: new DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant:false,
                    IsElder:false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer:false,
                    IsSpecialPioneer:false,
                    IsGroupServiceOverseer:false,
                    IsGroupServiceDeputyOverseer:false,
                    CanMobileCart:true}},

            {Id: 14,IsDelete:false,CanLogin:false,CanLoginToPublisherPanel:false, IsOnlyLogin:false, Login:'',
                Sex:SexEnum.Male, Name: 'Jeremiasz', Surname: 'Późny',
                DateOfBirth: new DateTime('1979-10-11').toJsDate(),DateOfBaptism: new DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant:false,
                    IsElder:false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer:false,
                    IsSpecialPioneer:false,
                    IsGroupServiceOverseer:false,
                    IsGroupServiceDeputyOverseer:false,
                    CanMobileCart:false}},
        ];

        this.Publishers = pubs;
    }

}

export class TeritoryGenState
{
    public Id:number;

}
