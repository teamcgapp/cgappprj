import {Component, Input, EventEmitter, Output, Inject, OnInit, ViewChild} from '@angular/core';
import {
    IPublisherService, ICgAppADOService, AuthorizationParams,
} from "../../services/cg.app.ado.common";

import 'rxjs/Rx';
import {Router} from "@angular/router";
import {PanelModule} from 'primeng/primeng';
import {DataTableModule,SharedModule} from 'primeng/primeng';
import {SelectItem} from "primeng/components/common/api";
import {Enumerable} from "typescript-dotnet-commonjs/System.Linq/Linq";
import {DateTime} from "typescript-dotnet-commonjs/System/Time/DateTime";
import {List} from "typescript-dotnet-commonjs/System/Collections/List";
import {PublisherModel, PrivilageModelEx, PrivilageModel} from "../../services/cg.app.model";

@Component({
    moduleId: module.id,
    selector: 'login-component',
    templateUrl: 'login.component.html'

})
export class UserLoginComponent
{
    constructor(@Inject(Router) private _router: Router, @Inject('ICgAppADOService')private _ado: ICgAppADOService) {

    }
    _login:string;
    _password:string;
    _msgs:Array<any>;

    doKeyUpPassword($event): void
    {
        if ($event.keyCode == 13){
            this.doLogin();
        }
    }

    onFillUser(val:string):void
    {
        this._login = val;
    }
    doLogin():void {

        let ap = new AuthorizationParams();
        ap.Login = this._login;
        ap.Password = this._password;
        this._ado.Authorization.AutorizeUser(ap).subscribe(
            (authorizationResult)=> {

                if (authorizationResult.AuthorizationStatus)
                {
                    this._router.navigate(["publisher_view"]);
                    //this._router.navigate(["publisher_view", {outlets: {'left': ['publisher_view_filter']}} ]);

                    this._msgs = [];
                }
                else
                {
                    this._msgs = [];
                    this._msgs.push({severity:'error', summary:'Authentication', detail:'Wrong login or password.'});
                }
            },
            (error)=> {

            }
        );

    }
}