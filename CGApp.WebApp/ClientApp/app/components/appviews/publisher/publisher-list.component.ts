import {Component, OnInit, ViewChild, Inject} from "@angular/core";
import {ICgAppADOService} from "../../../services/cg.app.ado.common";
import {Router} from "@angular/router";
import {List} from "typescript-dotnet-commonjs/System/Collections/List";
import {Enumerable} from "typescript-dotnet-commonjs/System.Linq/Linq";

import {PublisherModel, PrivilageModelEx} from "../../../services/cg.app.model";
import {PublisherListFilter} from "./publisher-list-filter.component";
import {PublisherDetailComponent} from "./publisher-detail.component"


@Component({
    moduleId: module.id,
    selector: 'publisher-list',
    templateUrl: 'publisher-list.component.html'

    })
export class PublisherListComponent implements OnInit
{
    _publishers:Array<PublisherModel>;
    @ViewChild('pubDetail') _publisherDetail:PublisherDetailComponent;
    private _selectedPublisher: PublisherModel;
    private _showDetail: boolean = false;


    constructor(@Inject("ICgAppADOService")private _adoService : ICgAppADOService,
                @Inject(Router) private _router:Router)
    {

    }

    onRowDblclick($event): void
    {
        //this._router.navigate(['publisher_view',$event.data.Id]);
        this._showDetail = true;
    }

    /*
    onChange_RefreshAfterFiltering():void {
        this.loadPublishers();
    }*/

    onFilterChange(publisherListFilter:PublisherListFilter):void {
        this.loadPublishers(publisherListFilter);
    }

    doBack(): void {
        this._showDetail = false;
    }

    doSave(): void {
        this._showDetail = false;
    }

    doCancel(): void {
        this._showDetail = false;
    }

    loadPublishers(publisherListFilter:PublisherListFilter = null): void {
        this._adoService.Publisher.getList().subscribe( (publishers)=>
        {
            let lst = Enumerable.from(publishers);
            if (publisherListFilter != null) {
                //filter publisherGroupId if some group was selected
                if (publisherListFilter.PublisherGroupsId.length > 0) {
                    var groups = new List(publisherListFilter.PublisherGroupsId);
                    lst = lst.where(
                        (x) => groups.contains(x.PublisherGroup.Id)
                    );
                }
            }
            // AND

            //filter privilage
            lst = lst.where(
                (x) => {

                    var res = false;

                    if (publisherListFilter == null)
                    {
                        res = true;
                    }
                    else if (PrivilageModelEx.IsAllUnset(publisherListFilter.Privilage)) {
                        res = true;
                    }
                    else{
                        if (publisherListFilter.Privilage.IsNotPublisher) {
                            res = res || x.Privilage.IsNotPublisher;
                        }
                        if (publisherListFilter.Privilage.IsUnbaptizedPublishers) {
                            res = res || x.Privilage.IsUnbaptizedPublishers;
                        }
                        if (publisherListFilter.Privilage.IsBaptizedPublishers) {
                            res = res || x.Privilage.IsBaptizedPublishers;
                        }
                        if (publisherListFilter.Privilage.CanMobileCart) {
                            res = res || x.Privilage.CanMobileCart;
                        }
                        if (publisherListFilter.Privilage.IsElder) {
                            res = res || x.Privilage.IsElder;
                        }
                        if (publisherListFilter.Privilage.IsMinisterialServant) {
                            res = res || x.Privilage.IsMinisterialServant;
                        }
                        if (publisherListFilter.Privilage.IsAuxiliaryPioneer) {
                            res = res || x.Privilage.IsAuxiliaryPioneer;
                        }
                        if (publisherListFilter.Privilage.IsRegularPioneer) {
                            res = res || x.Privilage.IsRegularPioneer;
                        }
                        if (publisherListFilter.Privilage.IsSpecialPioneer) {
                            res = res || x.Privilage.IsSpecialPioneer;
                        }
                        if (publisherListFilter.Privilage.IsGroupServiceOverseer) {
                            res = res || x.Privilage.IsGroupServiceOverseer;
                        }
                        if (publisherListFilter.Privilage.IsGroupServiceDeputyOverseer) {
                            res = res || x.Privilage.IsGroupServiceDeputyOverseer;
                        }
                    }
                    return res;

                });
            this._publishers = lst.toArray();
        },(error)=>
        {
            console.log(error);
        });
    }

    ngOnInit(){

        this.loadPublishers();
    }

    doSelect(publisher:PublisherModel):void {
        this._publisherDetail.setSelectedPublisher(publisher);
    }
}

