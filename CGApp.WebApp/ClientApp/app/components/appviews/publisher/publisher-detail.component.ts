import {Component, Input, EventEmitter, Output, Inject, OnInit, ViewChild} from '@angular/core';
import {
    IPublisherService, ICgAppADOService, AuthorizationParams,
} from "../../../services/cg.app.ado.common";

import 'rxjs/Rx';
import {Router} from "@angular/router";
import {PanelModule} from 'primeng/primeng';
import {DataTableModule,SharedModule} from 'primeng/primeng';
import {SelectItem} from "primeng/components/common/api";
import {Enumerable} from "typescript-dotnet-commonjs/System.Linq/Linq";
import {DateTime} from "typescript-dotnet-commonjs/System/Time/DateTime";
import {List} from "typescript-dotnet-commonjs/System/Collections/List";
import {PublisherModel, PrivilageModelEx, PrivilageModel} from "../../../services/cg.app.model";

@Component({
    moduleId: module.id,
    selector: 'publisher-detail',
    templateUrl: 'publisher-detail.component.html'
    })
export class PublisherDetailComponent
{
    @Input() Publisher: PublisherModel;
    @Output("back") Back = new EventEmitter();
    @Output("save") Save = new EventEmitter();
    @Output("cancel") Cancel = new EventEmitter();

    public setSelectedPublisher(publisher: PublisherModel):void
    {
        this.Publisher = publisher;
    }



    constructor() {
    }

    doBack(): void {
        this.Back.emit();
    }

    doSave(): void {
        this.Save.emit();
    }

    doCancel(): void {
        this.Cancel.emit();
    }

}
