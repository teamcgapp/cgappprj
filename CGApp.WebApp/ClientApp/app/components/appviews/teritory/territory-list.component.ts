import {Component, Input, EventEmitter, Output, Inject, OnInit, ViewChild} from '@angular/core';
import {
    IPublisherService, ICgAppADOService, AuthorizationParams,
} from "../../../services/cg.app.ado.common";

import 'rxjs/Rx';
import {Router} from "@angular/router";
import {PanelModule} from 'primeng/primeng';
import {DataTableModule,SharedModule} from 'primeng/primeng';
import {SelectItem} from "primeng/components/common/api";
import {Enumerable} from "typescript-dotnet-commonjs/System.Linq/Linq";
import {DateTime} from "typescript-dotnet-commonjs/System/Time/DateTime";
import {List} from "typescript-dotnet-commonjs/System/Collections/List";
import {PublisherModel, PrivilageModelEx, PrivilageModel, TeritoryModel} from "../../../services/cg.app.model";
import {TerritoryListFilter} from "./territory-list-filter.component";

@Component({
    moduleId: module.id,
    selector: 'territories-list',
    templateUrl: 'territory-list.component.html'
})
export class TerritoryListComponent{

    private _teritories: Array<TeritoryModel>;
    private _selectedTerritory: TeritoryModel;

    constructor(@Inject("ICgAppADOService")private _adoService : ICgAppADOService,
                @Inject(Router) private _router:Router)
    {

    }

    private getPubName(teritoryModel: TeritoryModel):string
    {
        if (teritoryModel.Publisher != null)
        {
            return teritoryModel.Publisher.Name + ' ' + teritoryModel.Publisher.Surname;
        }
        else
        {
            return '';
        }
    }

    onFilterChange(territoryListFilter:TerritoryListFilter):void {
        this.loadTeritories(territoryListFilter);
    }


    ngOnInit(){
        this.loadTeritories();
    }

    private loadTeritories(territoryFilter: TerritoryListFilter=null):void{

        this._adoService.Theritory.getList().subscribe( (teritories)=> {
            let lst = Enumerable.from(teritories);

            if (territoryFilter != null) {
                //filter publisherGroupId if some group was selected
                if (territoryFilter.PublisherGroupsId.length > 0) {
                    var groups = new List(territoryFilter.PublisherGroupsId);
                    lst = lst.where(
                        (x) =>
                        {
                            if (x.PublisherGroup == null)
                            {
                             return false;
                            }
                            else {
                                return groups.contains(x.PublisherGroup.Id);
                            }
                        }
                    );
                }
            }
            this._teritories = lst.toArray();


        },(error)=>
        {
            console.log(error);
        });
    }


}