import {Component, Input, EventEmitter, Output, Inject, OnInit, ViewChild} from '@angular/core';
import {
    IPublisherService, ICgAppADOService, AuthorizationParams,
} from "../../../services/cg.app.ado.common";

import 'rxjs/Rx';
import {Router} from "@angular/router";
import {PanelModule} from 'primeng/primeng';
import {DataTableModule,SharedModule} from 'primeng/primeng';
import {SelectItem} from "primeng/components/common/api";
import {Enumerable} from "typescript-dotnet-commonjs/System.Linq/Linq";
import {DateTime} from "typescript-dotnet-commonjs/System/Time/DateTime";
import {List} from "typescript-dotnet-commonjs/System/Collections/List";
import {PublisherModel, PrivilageModelEx, PrivilageModel} from "../../../services/cg.app.model";

@Component({
    moduleId: module.id,
    selector: 'territory-list-filter',
    templateUrl: 'territory-list-filter.component.html'

})
export class TerritoryListFilterComponent implements OnInit
{
    _publisherGroupsAsSelectItem:SelectItem[] = [];
    _selectedGroups:string[] = [];
    @Output() OnFilterChange = new EventEmitter();

    constructor(@Inject("ICgAppADOService")private _adoService : ICgAppADOService) {
    }

    ngOnInit(){
        this._adoService.PublisherGroup.getList().subscribe((publisherGroups)=>
        {
            publisherGroups.forEach((item)=>
            {
                this._publisherGroupsAsSelectItem.push( {label:item.Name,value:item.Id} );
            });

        },(error)=>
        {
            console.log(error);
        });
    }

    onChange_RefreshAfterFiltering():void
    {
        let filter = new TerritoryListFilter();
        filter.PublisherGroupsId = [];

        for(var sg in this._selectedGroups)
        {
            var groupId = Number(this._selectedGroups[sg]);
            filter.PublisherGroupsId.push(groupId);
        }
        this.OnFilterChange.emit(filter);
    }
}

export class TerritoryListFilter
{
    PublisherGroupsId: number[];
}
