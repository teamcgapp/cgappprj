import {Component, Input, EventEmitter, Output, Inject, OnInit, ViewChild} from '@angular/core';
import {
    IPublisherService, ICgAppADOService, AuthorizationParams,
} from "../../services/cg.app.ado.common";

import 'rxjs/Rx';
import {Router} from "@angular/router";
import {PanelModule} from 'primeng/primeng';
import {DataTableModule,SharedModule} from 'primeng/primeng';
import {SelectItem} from "primeng/components/common/api";
import {Enumerable} from "typescript-dotnet-commonjs/System.Linq/Linq";
import {DateTime} from "typescript-dotnet-commonjs/System/Time/DateTime";
import {List} from "typescript-dotnet-commonjs/System/Collections/List";
import {PublisherModel, PrivilageModelEx, PrivilageModel} from "../../services/cg.app.model";

@Component({
    moduleId: module.id,
    selector: 'main-toolbar',
    templateUrl: 'main-toolbar.component.html'

})
export class MainToolbar {
    _userInfo:string;
    constructor(@Inject("ICgAppADOService")private _adoService : ICgAppADOService) {
        this._userInfo = this._adoService.AuthorizationState.Login;
    }
}
