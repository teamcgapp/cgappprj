import {Component, Input, EventEmitter, Output, Inject, OnInit, ViewChild} from '@angular/core';
import {
    IPublisherService, ICgAppADOService, AuthorizationParams,
} from "../../services/cg.app.ado.common";

import 'rxjs/Rx';
import {Router} from "@angular/router";
import {PanelModule} from 'primeng/primeng';
import {DataTableModule,SharedModule} from 'primeng/primeng';
import {SelectItem} from "primeng/components/common/api";
import {Enumerable} from "typescript-dotnet-commonjs/System.Linq/Linq";
import {DateTime} from "typescript-dotnet-commonjs/System/Time/DateTime";
import {List} from "typescript-dotnet-commonjs/System/Collections/List";
import {PublisherModel, PrivilageModelEx, PrivilageModel} from "../../services/cg.app.model";

@Component({
    moduleId: module.id,
    selector: 'nav-panel',
    templateUrl: 'nav-panel.component.html'

})
export class NavigationPanelComponent implements OnInit
{
    private items;//: MenuItem[];
    constructor(@Inject(Router) private _router: Router,@Inject("ICgAppADOService")private _adoService : ICgAppADOService) {}

    ngOnInit() {
        this.items = [
            {
                label: 'Account',
                items: [
                    {label: 'Profile'},
                    {label: 'Logout',command: ()=>
                    {
                        this._adoService.Authorization.Logout();
                        this._router.navigate(['/login']);

                    }}
                ]
            },
            {
                label: 'Publishers',
                items: [
                    {label: 'Home',  routerLink:['home']},
                    {label: 'Publishers',  routerLink:['/publisher_view']},
                    {label: 'Territories',  routerLink:['/territory_view']}
                    //,



                    //{label: 'Publishers2',  routerLink:['publisher_view',{outlets: {'left': ['publisher_view_filter']}} ]}

                ]
            }];
    }
}
