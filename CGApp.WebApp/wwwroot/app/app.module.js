"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var animations_1 = require("@angular/platform-browser/animations");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var cg_app_ado_mockup_1 = require("./services/cg.app.ado.mockup");
//import {Observable} from "rxjs/Observable";
var primeng_1 = require("primeng/primeng");
var primeng_2 = require("primeng/primeng");
var primeng_3 = require("primeng/primeng");
var primeng_4 = require("primeng/primeng");
var primeng_5 = require("primeng/primeng");
var primeng_6 = require("primeng/primeng");
var primeng_7 = require("primeng/primeng");
var primeng_8 = require("primeng/primeng");
var primeng_9 = require("primeng/primeng");
var primeng_10 = require("primeng/primeng");
var primeng_11 = require("primeng/primeng");
var primeng_12 = require("primeng/primeng");
var primeng_13 = require("primeng/primeng");
var main_view_component_1 = require("./components/main/main-view.component");
var home_view_component_1 = require("./components/appviews/home/home-view.component");
var publisher_list_component_1 = require("./components/appviews/publisher/publisher-list.component");
var publisher_list_filter_component_1 = require("./components/appviews/publisher/publisher-list-filter.component");
var app_component_1 = require("./components/main/app.component");
var publisher_detail_component_1 = require("./components/appviews/publisher/publisher-detail.component");
var login_component_1 = require("./components/login/login.component");
var main_toolbar_component_1 = require("./components/main/main-toolbar.component");
var nav_panel_component_1 = require("./components/main/nav-panel.component");
var panel_confirmation_component_1 = require("./components/main/panel-confirmation.component");
var territory_list_component_1 = require("./components/appviews/teritory/territory-list.component");
var territory_list_filter_component_1 = require("./components/appviews/teritory/territory-list-filter.component");
exports.ROUTES = [
    { path: 'login', component: login_component_1.UserLoginComponent },
    { path: 'home', component: main_view_component_1.MainView, data: { panelTitle: 'List of publisher' },
        children: [
            { path: '', component: home_view_component_1.HomeViewComponent }
        ]
    },
    { path: 'publisher_view', component: main_view_component_1.MainView,
        children: [
            { path: '', component: publisher_list_component_1.PublisherListComponent, data: { panelTitle: 'List of publisher' } },
            { path: ':id', component: publisher_detail_component_1.PublisherDetailComponent, data: { panelTitle: 'List of publisher' } }
        ]
    },
    { path: 'territory_view', component: main_view_component_1.MainView, data: { panelTitle: 'List of teritories' },
        children: [
            { path: '', component: territory_list_component_1.TerritoryListComponent }
        ]
    },
    { path: '', redirectTo: 'login', pathMatch: 'prefix' },
    ///{path: '', redirectTo:'publisher_view/(left:publisher_view_filter)' , pathMatch:'prefix'},
    { path: '**', redirectTo: 'publisher_view', pathMatch: 'prefix' }
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            animations_1.BrowserAnimationsModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            router_1.RouterModule.forRoot(exports.ROUTES),
            primeng_1.PanelModule,
            primeng_2.DataTableModule,
            primeng_2.SharedModule,
            primeng_3.MenuModule,
            primeng_4.PasswordModule,
            primeng_5.InputTextModule,
            primeng_13.CalendarModule,
            primeng_6.MessagesModule,
            primeng_7.MenubarModule,
            primeng_8.ToolbarModule,
            primeng_12.ButtonModule,
            primeng_9.FieldsetModule,
            primeng_10.CheckboxModule,
            primeng_11.MultiSelectModule
        ],
        declarations: [app_component_1.AppComponent,
            home_view_component_1.HomeViewComponent,
            main_toolbar_component_1.MainToolbar,
            nav_panel_component_1.NavigationPanelComponent,
            main_view_component_1.MainView,
            publisher_list_component_1.PublisherListComponent, publisher_list_filter_component_1.PublisherListFilterComponent, publisher_detail_component_1.PublisherDetailComponent,
            territory_list_component_1.TerritoryListComponent, territory_list_filter_component_1.TerritoryListFilterComponent,
            login_component_1.UserLoginComponent, panel_confirmation_component_1.PanelConfirmationComponent],
        bootstrap: [app_component_1.AppComponent],
        providers: [{ provide: "ICgAppADOService", useClass: cg_app_ado_mockup_1.CgAppADOServiceMockup }]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map