"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
require("rxjs/Rx");
var router_1 = require("@angular/router");
var NavigationPanelComponent = (function () {
    function NavigationPanelComponent(_router, _adoService) {
        this._router = _router;
        this._adoService = _adoService;
    }
    NavigationPanelComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.items = [
            {
                label: 'Account',
                items: [
                    { label: 'Profile' },
                    { label: 'Logout', command: function () {
                            _this._adoService.Authorization.Logout();
                            _this._router.navigate(['/login']);
                        } }
                ]
            },
            {
                label: 'Publishers',
                items: [
                    { label: 'Home', routerLink: ['home'] },
                    { label: 'Publishers', routerLink: ['/publisher_view'] },
                    { label: 'Territories', routerLink: ['/territory_view'] }
                ]
            }
        ];
    };
    return NavigationPanelComponent;
}());
NavigationPanelComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'nav-panel',
        templateUrl: 'nav-panel.component.html'
    }),
    __param(0, core_1.Inject(router_1.Router)), __param(1, core_1.Inject("ICgAppADOService")),
    __metadata("design:paramtypes", [router_1.Router, Object])
], NavigationPanelComponent);
exports.NavigationPanelComponent = NavigationPanelComponent;
//# sourceMappingURL=nav-panel.component.js.map