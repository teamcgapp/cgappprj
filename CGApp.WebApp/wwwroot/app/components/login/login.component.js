"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var cg_app_ado_common_1 = require("../../services/cg.app.ado.common");
require("rxjs/Rx");
var router_1 = require("@angular/router");
var UserLoginComponent = (function () {
    function UserLoginComponent(_router, _ado) {
        this._router = _router;
        this._ado = _ado;
    }
    UserLoginComponent.prototype.doKeyUpPassword = function ($event) {
        if ($event.keyCode == 13) {
            this.doLogin();
        }
    };
    UserLoginComponent.prototype.onFillUser = function (val) {
        this._login = val;
    };
    UserLoginComponent.prototype.doLogin = function () {
        var _this = this;
        var ap = new cg_app_ado_common_1.AuthorizationParams();
        ap.Login = this._login;
        ap.Password = this._password;
        this._ado.Authorization.AutorizeUser(ap).subscribe(function (authorizationResult) {
            if (authorizationResult.AuthorizationStatus) {
                _this._router.navigate(["publisher_view"]);
                //this._router.navigate(["publisher_view", {outlets: {'left': ['publisher_view_filter']}} ]);
                _this._msgs = [];
            }
            else {
                _this._msgs = [];
                _this._msgs.push({ severity: 'error', summary: 'Authentication', detail: 'Wrong login or password.' });
            }
        }, function (error) {
        });
    };
    return UserLoginComponent;
}());
UserLoginComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'login-component',
        templateUrl: 'login.component.html'
    }),
    __param(0, core_1.Inject(router_1.Router)), __param(1, core_1.Inject('ICgAppADOService')),
    __metadata("design:paramtypes", [router_1.Router, Object])
], UserLoginComponent);
exports.UserLoginComponent = UserLoginComponent;
//# sourceMappingURL=login.component.js.map