"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
require("rxjs/Rx");
var cg_app_model_1 = require("../../../services/cg.app.model");
var PublisherDetailComponent = (function () {
    function PublisherDetailComponent() {
        this.Back = new core_1.EventEmitter();
        this.Save = new core_1.EventEmitter();
        this.Cancel = new core_1.EventEmitter();
    }
    PublisherDetailComponent.prototype.setSelectedPublisher = function (publisher) {
        this.Publisher = publisher;
    };
    PublisherDetailComponent.prototype.doBack = function () {
        this.Back.emit();
    };
    PublisherDetailComponent.prototype.doSave = function () {
        this.Save.emit();
    };
    PublisherDetailComponent.prototype.doCancel = function () {
        this.Cancel.emit();
    };
    return PublisherDetailComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", cg_app_model_1.PublisherModel)
], PublisherDetailComponent.prototype, "Publisher", void 0);
__decorate([
    core_1.Output("back"),
    __metadata("design:type", Object)
], PublisherDetailComponent.prototype, "Back", void 0);
__decorate([
    core_1.Output("save"),
    __metadata("design:type", Object)
], PublisherDetailComponent.prototype, "Save", void 0);
__decorate([
    core_1.Output("cancel"),
    __metadata("design:type", Object)
], PublisherDetailComponent.prototype, "Cancel", void 0);
PublisherDetailComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'publisher-detail',
        templateUrl: 'publisher-detail.component.html'
    }),
    __metadata("design:paramtypes", [])
], PublisherDetailComponent);
exports.PublisherDetailComponent = PublisherDetailComponent;
//# sourceMappingURL=publisher-detail.component.js.map