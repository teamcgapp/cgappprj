"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var List_1 = require("typescript-dotnet-commonjs/System/Collections/List");
var Linq_1 = require("typescript-dotnet-commonjs/System.Linq/Linq");
var cg_app_model_1 = require("../../../services/cg.app.model");
var publisher_detail_component_1 = require("./publisher-detail.component");
var PublisherListComponent = (function () {
    function PublisherListComponent(_adoService, _router) {
        this._adoService = _adoService;
        this._router = _router;
        this._showDetail = false;
    }
    PublisherListComponent.prototype.onRowDblclick = function ($event) {
        //this._router.navigate(['publisher_view',$event.data.Id]);
        this._showDetail = true;
    };
    /*
    onChange_RefreshAfterFiltering():void {
        this.loadPublishers();
    }*/
    PublisherListComponent.prototype.onFilterChange = function (publisherListFilter) {
        this.loadPublishers(publisherListFilter);
    };
    PublisherListComponent.prototype.doBack = function () {
        this._showDetail = false;
    };
    PublisherListComponent.prototype.doSave = function () {
        this._showDetail = false;
    };
    PublisherListComponent.prototype.doCancel = function () {
        this._showDetail = false;
    };
    PublisherListComponent.prototype.loadPublishers = function (publisherListFilter) {
        var _this = this;
        if (publisherListFilter === void 0) { publisherListFilter = null; }
        this._adoService.Publisher.getList().subscribe(function (publishers) {
            var lst = Linq_1.Enumerable.from(publishers);
            if (publisherListFilter != null) {
                //filter publisherGroupId if some group was selected
                if (publisherListFilter.PublisherGroupsId.length > 0) {
                    var groups = new List_1.List(publisherListFilter.PublisherGroupsId);
                    lst = lst.where(function (x) { return groups.contains(x.PublisherGroup.Id); });
                }
            }
            // AND
            //filter privilage
            lst = lst.where(function (x) {
                var res = false;
                if (publisherListFilter == null) {
                    res = true;
                }
                else if (cg_app_model_1.PrivilageModelEx.IsAllUnset(publisherListFilter.Privilage)) {
                    res = true;
                }
                else {
                    if (publisherListFilter.Privilage.IsNotPublisher) {
                        res = res || x.Privilage.IsNotPublisher;
                    }
                    if (publisherListFilter.Privilage.IsUnbaptizedPublishers) {
                        res = res || x.Privilage.IsUnbaptizedPublishers;
                    }
                    if (publisherListFilter.Privilage.IsBaptizedPublishers) {
                        res = res || x.Privilage.IsBaptizedPublishers;
                    }
                    if (publisherListFilter.Privilage.CanMobileCart) {
                        res = res || x.Privilage.CanMobileCart;
                    }
                    if (publisherListFilter.Privilage.IsElder) {
                        res = res || x.Privilage.IsElder;
                    }
                    if (publisherListFilter.Privilage.IsMinisterialServant) {
                        res = res || x.Privilage.IsMinisterialServant;
                    }
                    if (publisherListFilter.Privilage.IsAuxiliaryPioneer) {
                        res = res || x.Privilage.IsAuxiliaryPioneer;
                    }
                    if (publisherListFilter.Privilage.IsRegularPioneer) {
                        res = res || x.Privilage.IsRegularPioneer;
                    }
                    if (publisherListFilter.Privilage.IsSpecialPioneer) {
                        res = res || x.Privilage.IsSpecialPioneer;
                    }
                    if (publisherListFilter.Privilage.IsGroupServiceOverseer) {
                        res = res || x.Privilage.IsGroupServiceOverseer;
                    }
                    if (publisherListFilter.Privilage.IsGroupServiceDeputyOverseer) {
                        res = res || x.Privilage.IsGroupServiceDeputyOverseer;
                    }
                }
                return res;
            });
            _this._publishers = lst.toArray();
        }, function (error) {
            console.log(error);
        });
    };
    PublisherListComponent.prototype.ngOnInit = function () {
        this.loadPublishers();
    };
    PublisherListComponent.prototype.doSelect = function (publisher) {
        this._publisherDetail.setSelectedPublisher(publisher);
    };
    return PublisherListComponent;
}());
__decorate([
    core_1.ViewChild('pubDetail'),
    __metadata("design:type", publisher_detail_component_1.PublisherDetailComponent)
], PublisherListComponent.prototype, "_publisherDetail", void 0);
PublisherListComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'publisher-list',
        templateUrl: 'publisher-list.component.html'
    }),
    __param(0, core_1.Inject("ICgAppADOService")),
    __param(1, core_1.Inject(router_1.Router)),
    __metadata("design:paramtypes", [Object, router_1.Router])
], PublisherListComponent);
exports.PublisherListComponent = PublisherListComponent;
//# sourceMappingURL=publisher-list.component.js.map