"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
require("rxjs/Rx");
var cg_app_model_1 = require("../../../services/cg.app.model");
var PublisherListFilterComponent = (function () {
    function PublisherListFilterComponent(_adoService) {
        this._adoService = _adoService;
        this._publisherGroupsAsSelectItem = [];
        this._selectedGroups = [];
        this.OnFilterChange = new core_1.EventEmitter();
        this._PrivilageModel = new cg_app_model_1.PrivilageModel();
    }
    PublisherListFilterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._adoService.PublisherGroup.getList().subscribe(function (publisherGroups) {
            publisherGroups.forEach(function (item) {
                _this._publisherGroupsAsSelectItem.push({ label: item.Name, value: item.Id });
            });
        }, function (error) {
            console.log(error);
        });
    };
    PublisherListFilterComponent.prototype.onChange_RefreshAfterFiltering = function () {
        var filter = new PublisherListFilter();
        filter.Privilage = this._PrivilageModel;
        filter.PublisherGroupsId = [];
        for (var sg in this._selectedGroups) {
            var groupId = Number(this._selectedGroups[sg]);
            filter.PublisherGroupsId.push(groupId);
        }
        this.OnFilterChange.emit(filter);
    };
    return PublisherListFilterComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PublisherListFilterComponent.prototype, "OnFilterChange", void 0);
PublisherListFilterComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'publisher-list-filter',
        templateUrl: 'publisher-list-filter.component.html'
    }),
    __param(0, core_1.Inject("ICgAppADOService")),
    __metadata("design:paramtypes", [Object])
], PublisherListFilterComponent);
exports.PublisherListFilterComponent = PublisherListFilterComponent;
var PublisherListFilter = (function () {
    function PublisherListFilter() {
    }
    return PublisherListFilter;
}());
exports.PublisherListFilter = PublisherListFilter;
//# sourceMappingURL=publisher-list-filter.component.js.map