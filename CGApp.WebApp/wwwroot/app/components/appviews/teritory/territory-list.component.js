"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
require("rxjs/Rx");
var router_1 = require("@angular/router");
var Linq_1 = require("typescript-dotnet-commonjs/System.Linq/Linq");
var List_1 = require("typescript-dotnet-commonjs/System/Collections/List");
var TerritoryListComponent = (function () {
    function TerritoryListComponent(_adoService, _router) {
        this._adoService = _adoService;
        this._router = _router;
    }
    TerritoryListComponent.prototype.getPubName = function (teritoryModel) {
        if (teritoryModel.Publisher != null) {
            return teritoryModel.Publisher.Name + ' ' + teritoryModel.Publisher.Surname;
        }
        else {
            return '';
        }
    };
    TerritoryListComponent.prototype.onFilterChange = function (territoryListFilter) {
        this.loadTeritories(territoryListFilter);
    };
    TerritoryListComponent.prototype.ngOnInit = function () {
        this.loadTeritories();
    };
    TerritoryListComponent.prototype.loadTeritories = function (territoryFilter) {
        var _this = this;
        if (territoryFilter === void 0) { territoryFilter = null; }
        this._adoService.Theritory.getList().subscribe(function (teritories) {
            var lst = Linq_1.Enumerable.from(teritories);
            if (territoryFilter != null) {
                //filter publisherGroupId if some group was selected
                if (territoryFilter.PublisherGroupsId.length > 0) {
                    var groups = new List_1.List(territoryFilter.PublisherGroupsId);
                    lst = lst.where(function (x) {
                        if (x.PublisherGroup == null) {
                            return false;
                        }
                        else {
                            return groups.contains(x.PublisherGroup.Id);
                        }
                    });
                }
            }
            _this._teritories = lst.toArray();
        }, function (error) {
            console.log(error);
        });
    };
    return TerritoryListComponent;
}());
TerritoryListComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'territories-list',
        templateUrl: 'territory-list.component.html'
    }),
    __param(0, core_1.Inject("ICgAppADOService")),
    __param(1, core_1.Inject(router_1.Router)),
    __metadata("design:paramtypes", [Object, router_1.Router])
], TerritoryListComponent);
exports.TerritoryListComponent = TerritoryListComponent;
//# sourceMappingURL=territory-list.component.js.map