"use strict";
var PublisherModel = (function () {
    function PublisherModel() {
    }
    return PublisherModel;
}());
exports.PublisherModel = PublisherModel;
var PrivilageModel = (function () {
    function PrivilageModel() {
    }
    return PrivilageModel;
}());
exports.PrivilageModel = PrivilageModel;
var TeritoryModel = (function () {
    function TeritoryModel() {
    }
    return TeritoryModel;
}());
exports.TeritoryModel = TeritoryModel;
var TeritoryAdressModel = (function () {
    function TeritoryAdressModel() {
    }
    return TeritoryAdressModel;
}());
exports.TeritoryAdressModel = TeritoryAdressModel;
var PrivilageModelEx = (function () {
    function PrivilageModelEx() {
    }
    PrivilageModelEx.ClearFlags = function (pivilageModel) {
        pivilageModel.IsNotPublisher = false;
        pivilageModel.IsUnbaptizedPublishers = false;
        pivilageModel.IsBaptizedPublishers = false;
        pivilageModel.IsMinisterialServant = false;
        pivilageModel.IsElder = false;
        pivilageModel.IsRegularPioneer = false;
        pivilageModel.IsAuxiliaryPioneer = false;
        pivilageModel.IsSpecialPioneer = false;
        pivilageModel.IsGroupServiceOverseer = false;
        pivilageModel.IsGroupServiceDeputyOverseer = false;
        pivilageModel.CanMobileCart = false;
    };
    PrivilageModelEx.IsAllUnset = function (pivilageModel) {
        return (!pivilageModel.IsNotPublisher) &&
            (!pivilageModel.IsUnbaptizedPublishers) &&
            (!pivilageModel.IsBaptizedPublishers) &&
            (!pivilageModel.IsMinisterialServant) &&
            (!pivilageModel.IsElder) &&
            (!pivilageModel.IsRegularPioneer) &&
            (!pivilageModel.IsAuxiliaryPioneer) &&
            (!pivilageModel.IsSpecialPioneer) &&
            (!pivilageModel.IsGroupServiceOverseer) &&
            (!pivilageModel.IsGroupServiceDeputyOverseer) &&
            (!pivilageModel.CanMobileCart);
    };
    return PrivilageModelEx;
}());
exports.PrivilageModelEx = PrivilageModelEx;
var PublisherGroupModel = (function () {
    function PublisherGroupModel() {
    }
    return PublisherGroupModel;
}());
exports.PublisherGroupModel = PublisherGroupModel;
var SexEnum;
(function (SexEnum) {
    SexEnum[SexEnum["Male"] = 0] = "Male";
    SexEnum[SexEnum["Female"] = 1] = "Female";
})(SexEnum = exports.SexEnum || (exports.SexEnum = {}));
;
//# sourceMappingURL=cg.app.model.js.map