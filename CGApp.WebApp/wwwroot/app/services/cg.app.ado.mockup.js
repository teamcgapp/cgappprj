"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
require("rxjs/Rx");
var Linq_1 = require("typescript-dotnet-commonjs/System.Linq/Linq");
var DateTime_1 = require("typescript-dotnet-commonjs/System/Time/DateTime");
var cg_app_model_1 = require("./cg.app.model");
var cg_app_ado_common_1 = require("./cg.app.ado.common");
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';
var PublisherServiceMockup = (function () {
    function PublisherServiceMockup(_adoService) {
        this._adoService = _adoService;
    }
    PublisherServiceMockup.prototype.getList = function () {
        var _this = this;
        var source = Observable_1.Observable.create(function (observer) {
            observer.next(_this._adoService.Publishers);
            observer.complete();
        });
        return source;
    };
    return PublisherServiceMockup;
}());
exports.PublisherServiceMockup = PublisherServiceMockup;
var TeritoryServiceMockup = (function () {
    function TeritoryServiceMockup(_adoService) {
        this._adoService = _adoService;
    }
    TeritoryServiceMockup.prototype.getList = function () {
        var _this = this;
        var source = Observable_1.Observable.create(function (observer) {
            observer.next(_this._adoService.Teritories);
            observer.complete();
        });
        return source;
    };
    return TeritoryServiceMockup;
}());
exports.TeritoryServiceMockup = TeritoryServiceMockup;
var PublisherGroupServiceMockup = (function () {
    function PublisherGroupServiceMockup(_adoService) {
        this._adoService = _adoService;
    }
    PublisherGroupServiceMockup.prototype.getList = function () {
        var _this = this;
        var source = Observable_1.Observable.create(function (observer) {
            observer.next(_this._adoService.PublisherGroups);
            observer.complete();
        });
        return source;
    };
    return PublisherGroupServiceMockup;
}());
exports.PublisherGroupServiceMockup = PublisherGroupServiceMockup;
var AuthorizationServiceMockup = (function () {
    function AuthorizationServiceMockup(_adoService) {
        this._adoService = _adoService;
    }
    AuthorizationServiceMockup.prototype.AutorizeUser = function (authorizationParams) {
        var publisher = null;
        Observable_1.Observable.from(this._adoService.Publishers).
            filter(function (p) { return p.Login == authorizationParams.Login; }).
            subscribe(function (x) { return publisher = x; });
        var res = new cg_app_ado_common_1.AuthorizationResult();
        res.AuthorizationStatus = false;
        if (publisher !== null) {
            if (authorizationParams.Password === 'abc') {
                res.AuthorizationStatus = true;
            }
        }
        if (res.AuthorizationStatus) {
            this._adoService.AuthorizationState.UserIsAuthorized = true;
            this._adoService.AuthorizationState.Login = authorizationParams.Login;
        }
        else {
            this._adoService.AuthorizationState.UserIsAuthorized = false;
            this._adoService.AuthorizationState.Login = null;
        }
        var source = Observable_1.Observable.create(function (observer) {
            observer.next(res);
            observer.complete();
        });
        return source;
    };
    AuthorizationServiceMockup.prototype.Logout = function () {
        this._adoService.AuthorizationState.UserIsAuthorized = false;
        this._adoService.AuthorizationState.Login = null;
        var source = Observable_1.Observable.create(function (observer) {
            observer.next(true);
            observer.complete();
        });
        return source;
    };
    return AuthorizationServiceMockup;
}());
exports.AuthorizationServiceMockup = AuthorizationServiceMockup;
var CgAppADOServiceMockup = (function () {
    function CgAppADOServiceMockup() {
        this.InitializeMockupData();
        this.AuthorizationState = new cg_app_ado_common_1.AuthorizationState();
        this._authorization = new AuthorizationServiceMockup(this);
        this._publisherService = new PublisherServiceMockup(this);
        this._publisherGroup = new PublisherGroupServiceMockup(this);
        this._teritoryService = new TeritoryServiceMockup(this);
        var val = new DateTime_1.DateTime('dd');
    }
    Object.defineProperty(CgAppADOServiceMockup.prototype, "Publisher", {
        get: function () {
            return this._publisherService;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CgAppADOServiceMockup.prototype, "PublisherGroup", {
        get: function () {
            return this._publisherGroup;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CgAppADOServiceMockup.prototype, "Authorization", {
        get: function () {
            return this._authorization;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CgAppADOServiceMockup.prototype, "Theritory", {
        get: function () {
            return this._teritoryService;
        },
        enumerable: true,
        configurable: true
    });
    CgAppADOServiceMockup.prototype.InitializeMockupData = function () {
        console.log("ABCD " + DateTime_1.DateTime.now.toString());
        this.gr1 = { Id: 1, Pos: 1, Name: 'Grupa 1' };
        this.gr2 = { Id: 2, Pos: 2, Name: 'Grupa 2' };
        this.gr3 = { Id: 3, Pos: 3, Name: 'Grupa 3' };
        this.gr4 = { Id: 4, Pos: 4, Name: 'Grupa 4' };
        this.PublisherGroups = [this.gr1, this.gr2, this.gr3, this.gr4];
        this.generatePublishers();
        this.generateTerytories();
    };
    CgAppADOServiceMockup.prototype.generateTerytories = function () {
        var ters = [];
        var teritoryGenState = new TeritoryGenState();
        teritoryGenState.Id = 0;
        this.addTeritories(ters, teritoryGenState, 135);
        this.attachPublisherToTeritories(ters, 1, 1, 25);
        this.attachPublisherToTeritories(ters, 2, 26, 50);
        this.attachPublisherToTeritories(ters, 3, 51, 75);
        this.attachPublisherToTeritories(ters, 4, 76, 100);
        this.Teritories = ters;
    };
    CgAppADOServiceMockup.prototype.attachPublisherToTeritories = function (ters, pubGroupId, terIdStart, terIdEnd) {
        var tersAsEnumerable = Linq_1.Enumerable.from(ters);
        ;
        var groupAsEnumerable = Linq_1.Enumerable.from(this.PublisherGroups);
        var pubAsEnumerable = Linq_1.Enumerable.from(this.Publishers);
        var ters_for_group = tersAsEnumerable.where(function (t) { return t.Id >= terIdStart && t.Id <= terIdEnd; });
        var pub_group = groupAsEnumerable.where(function (g) { return g.Id == pubGroupId; }).singleOrDefault();
        var tersIndex = 0;
        ters_for_group.forEach(function (t) {
            t.PublisherGroup = pub_group;
        });
        var pub_for_group = pubAsEnumerable.where(function (p) { return p.PublisherGroup.Id == pubGroupId; });
        var ters_for_group_array = ters_for_group.toArray();
        pub_for_group.forEach(function (pub) {
            ters_for_group_array[tersIndex].Publisher = pub;
            tersIndex++;
        });
    };
    CgAppADOServiceMockup.prototype.addTeritories = function (teritories, teritoryGenState, count) {
        for (var i = 0; i < count; i++) {
            teritoryGenState.Id = teritoryGenState.Id + 1;
            teritories.push({
                Id: teritoryGenState.Id,
                Name: 'Teritory - ' + teritoryGenState.Id,
                Streets: new Array(),
                Description: '',
                PublisherGroup: null,
                Publisher: null
            });
        }
    };
    CgAppADOServiceMockup.prototype.generatePublishers = function () {
        var pubs = [
            { Id: 1, IsDelete: false, CanLogin: true, CanLoginToPublisherPanel: false, IsOnlyLogin: false, Login: 'kamil.predki',
                Sex: cg_app_model_1.SexEnum.Male, Name: 'Kamil', Surname: 'Prędki',
                DateOfBirth: new DateTime_1.DateTime("1979-10-11").toJsDate(), DateOfBaptism: new DateTime_1.DateTime("1991-01-05").toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: false,
                    IsElder: true,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer: false,
                    IsSpecialPioneer: false,
                    IsGroupServiceOverseer: true,
                    IsGroupServiceDeputyOverseer: false,
                    CanMobileCart: true
                } },
            { Id: 2, IsDelete: false, CanLogin: false, CanLoginToPublisherPanel: false, IsOnlyLogin: false, Login: '',
                Sex: cg_app_model_1.SexEnum.Female, Name: 'Anna', Surname: 'Prędki',
                DateOfBirth: new DateTime_1.DateTime('1981-11-13').toJsDate(), DateOfBaptism: new DateTime_1.DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: false,
                    IsElder: false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer: false,
                    IsSpecialPioneer: false,
                    IsGroupServiceOverseer: false,
                    IsGroupServiceDeputyOverseer: false,
                    CanMobileCart: true
                } },
            { Id: 3, IsDelete: false, CanLogin: true, CanLoginToPublisherPanel: false, IsOnlyLogin: false, Login: 'janusz.predki',
                Sex: cg_app_model_1.SexEnum.Male, Name: 'Janusz', Surname: 'Prędki',
                DateOfBirth: new DateTime_1.DateTime('1990-01-15').toJsDate(), DateOfBaptism: new DateTime_1.DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: true,
                    IsElder: false,
                    IsRegularPioneer: true,
                    IsAuxiliaryPioneer: false,
                    IsSpecialPioneer: false,
                    IsGroupServiceOverseer: false,
                    IsGroupServiceDeputyOverseer: false,
                    CanMobileCart: true
                } },
            { Id: 4, IsDelete: false, CanLogin: true, CanLoginToPublisherPanel: false, IsOnlyLogin: false, Login: 'szymon.sprytny',
                Sex: cg_app_model_1.SexEnum.Male, Name: 'Szymon', Surname: 'Sprytny',
                DateOfBirth: new DateTime_1.DateTime('1985-04-05').toJsDate(), DateOfBaptism: new DateTime_1.DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr2,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: false,
                    IsElder: true,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer: false,
                    IsSpecialPioneer: false,
                    IsGroupServiceOverseer: true,
                    IsGroupServiceDeputyOverseer: false,
                    CanMobileCart: true
                } },
            { Id: 5, IsDelete: false, CanLogin: false, CanLoginToPublisherPanel: false, IsOnlyLogin: false, Login: '',
                Sex: cg_app_model_1.SexEnum.Female, Name: 'Katarzyna', Surname: 'Sprytny',
                DateOfBirth: new DateTime_1.DateTime('1979-10-11').toJsDate(), DateOfBaptism: new DateTime_1.DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr2,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: false,
                    IsElder: false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer: false,
                    IsSpecialPioneer: false,
                    IsGroupServiceOverseer: false,
                    IsGroupServiceDeputyOverseer: false,
                    CanMobileCart: true
                } },
            { Id: 6, IsDelete: false, CanLogin: true, CanLoginToPublisherPanel: false, IsOnlyLogin: false, Login: 'tomasz.gorliwy',
                Sex: cg_app_model_1.SexEnum.Male, Name: 'Tomasz', Surname: 'Gorliwy',
                DateOfBirth: new DateTime_1.DateTime('1979-10-11').toJsDate(), DateOfBaptism: new DateTime_1.DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr2,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: true,
                    IsElder: false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer: false,
                    IsSpecialPioneer: false,
                    IsGroupServiceOverseer: false,
                    IsGroupServiceDeputyOverseer: false,
                    CanMobileCart: true
                } },
            { Id: 7, IsDelete: false, CanLogin: false, CanLoginToPublisherPanel: false, IsOnlyLogin: false, Login: '',
                Sex: cg_app_model_1.SexEnum.Female, Name: 'Marta', Surname: 'Gorliwy',
                PublisherGroup: this.gr2,
                DateOfBirth: new DateTime_1.DateTime('1979-10-11').toJsDate(), DateOfBaptism: new DateTime_1.DateTime('1991-01-05').toJsDate(),
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: false,
                    IsElder: false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer: false,
                    IsSpecialPioneer: false,
                    IsGroupServiceOverseer: false,
                    IsGroupServiceDeputyOverseer: false,
                    CanMobileCart: true
                } },
            { Id: 8, IsDelete: false, CanLogin: true, CanLoginToPublisherPanel: false, IsOnlyLogin: false, Login: 'mateusz.pomyslowy',
                Sex: cg_app_model_1.SexEnum.Male, Name: 'Mateusz', Surname: 'Pomysłowy',
                DateOfBirth: new DateTime_1.DateTime('1979-10-11').toJsDate(), DateOfBaptism: new DateTime_1.DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr2,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: true,
                    IsElder: false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer: false,
                    IsSpecialPioneer: false,
                    IsGroupServiceOverseer: false,
                    IsGroupServiceDeputyOverseer: false,
                    CanMobileCart: true
                } },
            { Id: 9, IsDelete: false, CanLogin: false, CanLoginToPublisherPanel: false, IsOnlyLogin: false, Login: '',
                Sex: cg_app_model_1.SexEnum.Female, Name: 'Joanna', Surname: 'Pomysłowy',
                DateOfBirth: new DateTime_1.DateTime('1979-10-11').toJsDate(), DateOfBaptism: new DateTime_1.DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr2,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: false,
                    IsElder: false,
                    IsRegularPioneer: true,
                    IsAuxiliaryPioneer: false,
                    IsSpecialPioneer: false,
                    IsGroupServiceOverseer: false,
                    IsGroupServiceDeputyOverseer: false,
                    CanMobileCart: true
                } },
            { Id: 10, IsDelete: false, CanLogin: false, CanLoginToPublisherPanel: false, IsOnlyLogin: false, Login: '',
                Sex: cg_app_model_1.SexEnum.Male, Name: 'Dobromir', Surname: 'Pomysłowy',
                DateOfBirth: new DateTime_1.DateTime('1979-10-11').toJsDate(), DateOfBaptism: new DateTime_1.DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: false,
                    IsElder: false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer: false,
                    IsSpecialPioneer: false,
                    IsGroupServiceOverseer: false,
                    IsGroupServiceDeputyOverseer: false,
                    CanMobileCart: true
                } },
            { Id: 11, IsDelete: false, CanLogin: false, CanLoginToPublisherPanel: false, IsOnlyLogin: false, Login: '',
                Sex: cg_app_model_1.SexEnum.Female, Name: 'Katarzyna', Surname: 'Pomysłowy',
                DateOfBirth: new DateTime_1.DateTime('1979-10-11').toJsDate(), DateOfBaptism: new DateTime_1.DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: false,
                    IsElder: false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer: true,
                    IsSpecialPioneer: false,
                    IsGroupServiceOverseer: false,
                    IsGroupServiceDeputyOverseer: false,
                    CanMobileCart: true
                } },
            { Id: 12, IsDelete: false, CanLogin: false, CanLoginToPublisherPanel: false, IsOnlyLogin: false, Login: '',
                Sex: cg_app_model_1.SexEnum.Female, Name: 'Joanna', Surname: 'Późny',
                DateOfBirth: new DateTime_1.DateTime('1979-10-11').toJsDate(), DateOfBaptism: new DateTime_1.DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: false,
                    IsElder: false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer: false,
                    IsSpecialPioneer: false,
                    IsGroupServiceOverseer: false,
                    IsGroupServiceDeputyOverseer: false,
                    CanMobileCart: true
                } },
            { Id: 13, IsDelete: false, CanLogin: false, CanLoginToPublisherPanel: false, IsOnlyLogin: false, Login: '',
                Sex: cg_app_model_1.SexEnum.Male, Name: 'Kamil', Surname: 'Późny',
                DateOfBirth: new DateTime_1.DateTime('1979-10-11').toJsDate(), DateOfBaptism: new DateTime_1.DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: false,
                    IsElder: false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer: false,
                    IsSpecialPioneer: false,
                    IsGroupServiceOverseer: false,
                    IsGroupServiceDeputyOverseer: false,
                    CanMobileCart: true
                } },
            { Id: 14, IsDelete: false, CanLogin: false, CanLoginToPublisherPanel: false, IsOnlyLogin: false, Login: '',
                Sex: cg_app_model_1.SexEnum.Male, Name: 'Jeremiasz', Surname: 'Późny',
                DateOfBirth: new DateTime_1.DateTime('1979-10-11').toJsDate(), DateOfBaptism: new DateTime_1.DateTime('1991-01-05').toJsDate(),
                PublisherGroup: this.gr1,
                Privilage: {
                    IsNotPublisher: false,
                    IsUnbaptizedPublishers: false,
                    IsBaptizedPublishers: true,
                    IsMinisterialServant: false,
                    IsElder: false,
                    IsRegularPioneer: false,
                    IsAuxiliaryPioneer: false,
                    IsSpecialPioneer: false,
                    IsGroupServiceOverseer: false,
                    IsGroupServiceDeputyOverseer: false,
                    CanMobileCart: false
                } },
        ];
        this.Publishers = pubs;
    };
    return CgAppADOServiceMockup;
}());
CgAppADOServiceMockup = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], CgAppADOServiceMockup);
exports.CgAppADOServiceMockup = CgAppADOServiceMockup;
var TeritoryGenState = (function () {
    function TeritoryGenState() {
    }
    return TeritoryGenState;
}());
exports.TeritoryGenState = TeritoryGenState;
//# sourceMappingURL=cg.app.ado.mockup.js.map