﻿using System;
using CGApp.WebAPI;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;


namespace CGApp.WebApp
{
    public class CGAppSessionCtx: ICGAppSessionCtx, ICGAppSessionCtxSessionAccessor
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session;
                     
        public int UserId { get => GetSessionValue_Int32("UserId"); set => SetSessionValue_Int32("UserId", value); }
        public int CongregationId { get => GetSessionValue_Int32("CongregationId"); set => SetSessionValue_Int32("CongregationId", value); }

        public ISession Session { get => _session; set => _session = value; }
        
        /*
        private void SetSessionValue_Int32(string key,int value)
        {
            _session.SetInt32(key, value);
        }

        private int GetSessionValue_Int32(string key)
        {
            var val = _session.GetInt32(key);
            return val ?? 0;
        } */

            
        private void SetSessionValue_Int32(string key, int value)
        {
            _session = _httpContextAccessor.HttpContext.Session;
            _session.SetInt32(key, value);
        }

        private int GetSessionValue_Int32(string key)
        {
            _session = _httpContextAccessor.HttpContext.Session;
            var val = _session.GetInt32(key);
            return val ?? 0;
        } 


        public CGAppSessionCtx(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void Invalidate()
        {
            _session = _httpContextAccessor.HttpContext.Session;
            _session.Clear();
        }
    }       
}

