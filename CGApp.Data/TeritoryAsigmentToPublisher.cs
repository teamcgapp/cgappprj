﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CGApp.Data
{
    public class TeritoryAsigmentToPublisher
    {
        [Key]
        [Required]
        public int Id { get; set; }

        public int TeritoryId { get; set; }

        [ForeignKey("TeritoryId")]
        public virtual Teritory Teritory { get; set; }

        public int AssignedToPublisherId { get; set; }

        [ForeignKey("AssignedToPublisherId")]
        public virtual Publisher AssignedToPublisher { get; set; }

        public DateTime StartAssigment { get; set; }
        public DateTime? EndAssigment { get; set; }

        public int AsgPublisherId { get; set; }

        [ForeignKey("AssignedByPublisherId")]
        public virtual Publisher AssignedByPublisher { get; set; }

        public virtual bool WithTeritoryCard { get; set; }

        public virtual bool IsNoted { get; set; }
        public int NotedByPublisherId { get; set; }

        [ForeignKey("NotedByPublisherId")]
        public virtual Publisher NotedByPublisher { get; set; }

        public bool IsActive { get; set; }

        public virtual bool IsCardReturned { get; set; }
    }
}
