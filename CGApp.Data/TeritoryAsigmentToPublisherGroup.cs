﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CGApp.Data
{
    public class TeritoryAsigmentToPublisherGroup
    {
        [Key]
        [Required]
        public int Id { get; set; }

        public int TeritoryId { get; set; }

        [ForeignKey("TeritoryId")]
        public virtual Teritory Teritory { get; set; }

        public int PublisherGroupId { get; set; }

        [ForeignKey("PublisherGroupId")]
        public virtual PublisherGroup PublisherGroup { get; set; }

        public DateTime StartAssigment { get; set; }
        public DateTime? EndAssigment { get; set; }
        
        public virtual bool WithTeritoryCard { get; set; }

        public virtual bool IsNoted { get; set; }
        public int NotedByPublisherId { get; set; }

        [ForeignKey("NotedByPublisherId")]
        public virtual Publisher NotedByPublisher { get; set; }

        public bool IsActive { get; set; }

        public virtual bool IsCardReturned { get; set; }
    }
}
