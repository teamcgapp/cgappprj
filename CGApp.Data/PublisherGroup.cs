﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CGApp.Data
{


    public class PublisherGroup
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [MaxLength(60)]
        public string InternalCode { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }
        public int Pos { get; set; }

        [ForeignKey("CongregationId")]
        public virtual Congregation Congregation { get; set; }

        public virtual int CongregationId { get; set; }

        public virtual List<Publisher> Publishers { get; set; }        
    }
}
