﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CGApp.Data
{
    public class Congregation
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [MaxLength(60)]
        public string InternalCode { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public virtual List<Publisher>  Publishers { get; set; }

        public virtual List<Teritory> Teritories { get; set; }

        public virtual List<PublisherGroup> PublisherGroups { get; set; }


    }
}
