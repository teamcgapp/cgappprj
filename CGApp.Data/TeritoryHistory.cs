﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CGApp.Data
{
    public class TeritoryCoveringHistory
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [ForeignKey("TeritoryId")]
        public virtual Teritory Teritory { get; set; }

        public int CoveredByPublisherId { get; set; }

        [ForeignKey("CoveredByPublisherId")]
        public virtual Publisher CoveredByPublisher { get; set; }



        public int CoveredByPublisherGroupId { get; set; }

        [ForeignKey("CoveredByPublisherGroupId")]
        public virtual PublisherGroup CoveredByPublisherGroup { get; set; }


        public DateTime? CoveredData { get; set; }

        [MaxLength(300)]
        public string Remarks { get; set; }


    }
}
