﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CGApp.Data
{
    public class Publisher
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [MaxLength(60)]
        public string InternalCode { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        [Required]
        [MaxLength(128)]
        public string Surname { get; set; }

        [MaxLength(128)]
        public string Login { get; set; }

        [MaxLength(128)]
        public string Password { get; set; }

        [MaxLength(128)]
        [EmailAddress]
        public string Email { get; set; }
        public bool IsDelete { get; set; }
        public bool IsOnlyLogin { get; set; }
        public bool CanLogin { get; set; }
        //public bool CanLoginToPublisherPanel { get; set; }
        
        public DateTime? DateOfBirth { get; set; }
        public DateTime? DateOfBaptism { get; set; }
        public SexEnum Sex { get; set; }
        //SexDesc: string;        
        public virtual Privilage Privilage { get; set; }
        public virtual PublisherGroup PublisherGroup { get; set; }

        [ForeignKey("CongregationId")]
        public virtual Congregation Congregation { get; set; }

        /*
        [InverseProperty("AssignedToPublisher")]
        public virtual List<TeritoryAsigmentToPublisher> TeritoriesAssignedToPublisher { get; set; }
        
        [InverseProperty("AssignedByPublisher")]
        public virtual List<TeritoryAsigmentToPublisher> TeritoriesAsigmentsByPublisher { get; set; }

        [InverseProperty("NotedByPublisher")]
        public virtual List<TeritoryAsigmentToPublisher> TeritoriesNotedByPublisher { get; set; }


        //================================

        
        public virtual List<TeritoryAsigmentToPublisherGroup> TeritoryAsigmentsToPublisherGroup { get; set; } */

        //public virtual List<LoginHistory> LoginHistories { get; set; }
        public int OrderViewInGroup { get; internal set; }        
    }
}
