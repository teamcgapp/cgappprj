﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CGApp.Data
{
    //[Table("LOGIN_HIST")]
    public class LoginHistory
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order= 0)]
        public int Id { get; set; }

        [ForeignKey("PublisherId")]
        [Column(Order = 1)]
        public Publisher Publisher { get; set; }

        [Column(Order = 2)]
        public DateTime LoginTime { get; set; }

        [Column(Order = 3)]
        public string Login { get; set; }

        [Column(Order = 4)]
        public bool? ParamsWasIncoret { get; set; }

        [Column(Order = 5)]
        public bool? LoginWasNotFound { get; set; }

        [Column(Order = 6)]
        public bool? IsToMuchLogins { get; set; }

        [Column(Order = 7)]
        public bool? IsCanLoginWasNotSet { get; set; }

        [Column(Order = 8)]
        public bool? PasswordWasWrong { get; set; }

        [Column(Order = 9)]
        public bool? CongregationWasNotSet { get; set; }

        [Column(Order = 10)]
        public bool? IsWasException { get; set; }

        [Column(Order = 11)]
        public bool IsOk { get; set; }

        [Column(Order = 12)]
        public string DataInfo { get; set; }

        [Column(Order = 13)]
        public string ExceptionMsg { get; set; }

    }
}





/*
public DateTime LoginTime { get; set; }        

public string Login { get; set; }
public bool? IsParamsCorrect { get; set; }
public bool? IsLoginWasNotFound { get; set; }                       
public bool? IsToMuchLogins { get; set; }
public bool? IsCanLoginWasNotSet { get; set; }
public bool? IsPasswordWasNotCorrect { get; set; }
public bool? IsCongregationWasNotSet { get; set; }
public bool? IsWasException { get; set; }
public bool IsOk { get; set; }
public string DataInfo { get; set; }
public string ExceptionMsg { get; set; }*/
