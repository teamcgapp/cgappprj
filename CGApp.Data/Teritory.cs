﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CGApp.Data
{
    public class Teritory
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }


        [MaxLength(60)]
        public string InternalCode { get; set; }

        [ForeignKey("CongregationId")]
        public virtual Congregation Congregation { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        [Required]
        [MaxLength(128)]
        public string Code { get; set; }

        public string Description { get; set; }       
        
        public virtual List<TeritoryAsigmentToPublisher> TeritoryAsigmentToPublisher { get; set; }

        public virtual List<TeritoryAsigmentToPublisherGroup> TeritoryAsigmentToPublisherGroup { get; set; }
        public int OrderView { get; internal set; }        
    }
}
