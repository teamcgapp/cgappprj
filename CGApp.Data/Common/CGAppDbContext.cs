﻿using Microsoft.EntityFrameworkCore;

namespace CGApp.Data
{
    public class CGAppDbContext: DbContext
    {

        public CGAppDbContext(DbContextOptions options):base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var publisherBuilder = modelBuilder.Entity<Publisher>();
            publisherBuilder.HasIndex(p => p.Login).IsUnique();
            publisherBuilder.HasIndex(p => p.Email);


            var publisherGroupBuilder = modelBuilder.Entity<PublisherGroup>();
            publisherGroupBuilder.HasIndex(p => p.Name).IsUnique();
            publisherGroupBuilder.HasIndex(p => new { p.Name, p.CongregationId }).IsUnique();



            var loginHistoryBuilder = modelBuilder.Entity<LoginHistory>();
            loginHistoryBuilder.HasIndex(p => p.Login);
            loginHistoryBuilder.HasIndex(p => p.LoginTime);

            var teritoryBuilder = modelBuilder.Entity<Teritory>();
            teritoryBuilder.HasIndex(p => p.InternalCode);
            teritoryBuilder.HasIndex(p => p.Code);
           
                

           //.HasRequired(f => f.)
           //.WithMany(f => f.)
           //.HasForeignKey(g => g.)
           //.WillCascadeOnDelete(false);



        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {                        
            base.OnConfiguring(optionsBuilder);

            
        }        
        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<Teritory> Teritories { get; set; }
        public DbSet<PublisherGroup> PublisherGroups { get; set; }

        public DbSet<Privilage> Privilages { get; set; }     
        
        public DbSet<Congregation> Congregations { get; set; }

        public DbSet<LoginHistory> LoginHistories { get; set; }

        public DbSet<TeritoryAsigmentToPublisher> TeritoryAsigmentsToPublisher { get; set; }
        public DbSet<TeritoryAsigmentToPublisherGroup> TeritoryAsigmentsToPublisherGroup { get; set; }


        public DbSet<TeritoryCoveringHistory> TeritoriesCoveringHistory { get; set; }


    }
}
