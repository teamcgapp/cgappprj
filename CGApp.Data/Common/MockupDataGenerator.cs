﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace CGApp.Data
{
    public class MockupDataGenerator : IMockupDataGenerator
    {
        private CGAppDbContext DbContext;
        private MockupDataHolder MockupDataHolder;

        public MockupDataGenerator(CGAppDbContext dbContext)
        {
            DbContext = dbContext;
            MockupDataHolder = new MockupDataHolder();
        }

        public void Generate()
        {
            var pubCount = (from p in DbContext.Publishers
                            select p).Count();
            if(pubCount > 0 )
            {
                return;
            }

            MockupDataHolder.DataForCongregationA.Congregation = 
                new Congregation()
                {
                    Name = "Congregation A",
                    Code = "A",
                    InternalCode = MockupConsta_CongregationA.CongregationCode
                };

            MockupDataHolder.DataForCongregationB.Congregation = 
                new Congregation()
                {
                    Name = "Congregation B",
                    Code = "B",
                    InternalCode = MockupConsta_CongregationB.CongregationCode
                };

            DbContext.Congregations.Add(MockupDataHolder.DataForCongregationA.Congregation);
            DbContext.Congregations.Add(MockupDataHolder.DataForCongregationB.Congregation);

            GeneratePublisherGroups_CongregationA(MockupDataHolder.DataForCongregationA);
            GeneratePublisher(MockupDataHolder.DataForCongregationA);
            GenerateTeritories(MockupDataHolder.DataForCongregationA);
            GenerateTeritories(MockupDataHolder.DataForCongregationB);
            
            DbContext.SaveChanges();
        }

        private void GeneratePublisherGroups_CongregationA(DataForCongregation dataForCongregation)
        {
            var cg = dataForCongregation.Congregation;
            
            dataForCongregation.PublisherGroup1 = new PublisherGroup()
            {
                Name = "Group 1",
                InternalCode = MockupConsta_CongregationA.PublisherGroup1,
                Congregation = cg,
                Pos =1
            };

            dataForCongregation.PublisherGroup2 = new PublisherGroup()
            {
                Name = "Group 2",
                InternalCode = MockupConsta_CongregationA.PublisherGroup2,
                Congregation = cg,
                Pos = 2
            };

            dataForCongregation.PublisherGroup3 = new PublisherGroup()
            {
                Name = "Group 3",
                InternalCode = MockupConsta_CongregationA.PublisherGroup3,
                Congregation = cg,
                Pos = 3
            };

            dataForCongregation.PublisherGroup4 = new PublisherGroup()
            {
                Name = "Group 4",
                InternalCode = MockupConsta_CongregationA.PublisherGroup4,
                Congregation = cg,
                Pos = 4
            };

            dataForCongregation.PublisherGroup5 = new PublisherGroup()
            {
                Name = "Group 5",
                InternalCode = MockupConsta_CongregationA.PublisherGroup5,
                Congregation = cg,
                Pos = 5
            };

            DbContext.PublisherGroups.Add(dataForCongregation.PublisherGroup1);
            DbContext.PublisherGroups.Add(dataForCongregation.PublisherGroup2);
            DbContext.PublisherGroups.Add(dataForCongregation.PublisherGroup3);
            DbContext.PublisherGroups.Add(dataForCongregation.PublisherGroup4);
            DbContext.PublisherGroups.Add(dataForCongregation.PublisherGroup5);

        }

        
        private void GeneratePublisher(DataForCongregation dataForCongregation)
        {
            var lst = new List<Publisher>();

            //GROUP 1
            var cg = dataForCongregation.Congregation;
            lst.Add(new Publisher(){
                    IsDelete = false,
                    CanLogin = true,
                    Login = "kszybki",
                    Password = "abc",
                    Email = "kamil.szybki@gmail.com",                    
                    Name = "Kamil",
                    Surname = "Szybki",
                    Congregation = cg,                    
                    Sex = SexEnum.Male,
                    PublisherGroup = dataForCongregation.PublisherGroup1,
                    DateOfBirth = DateTime.Parse("1985-02-01"),
                    DateOfBaptism = DateTime.Parse("2001-01-10"),          
                    OrderViewInGroup = 1,
                Privilage = new Privilage()
                    {
                      PublisherPrivilage = PublisherPrivilage.BaptizedPublisher,
                      PionierPrivilage = PionierPrivilage.No,
                      CongregationPrivilage  = CongregationPrivilage.Elder,
                      GroupServiceOverseerPrivilage = GroupServiceOverseerPrivilage.GroupServiceOverseer,
                      MobileCartPrivilage = MobileCartPrivilage.LocalMobileCartPrivilage                                            
                    }
            });

            
            lst.Add(new Publisher()
            {
                IsDelete = false,
                CanLogin = true,
                Login = "jfajnisty",
                Password = "abc",
                Email = "janusz.fajnisty@gmail.com",
                Name = "Janusz",
                Surname = "Fajnisty",
                Congregation = cg,
                Sex = SexEnum.Male,
                PublisherGroup = dataForCongregation.PublisherGroup1,
                Privilage = new Privilage()
                {
                    PublisherPrivilage = PublisherPrivilage.BaptizedPublisher,
                    PionierPrivilage = PionierPrivilage.No,
                    CongregationPrivilage = CongregationPrivilage.MinisterialServant,
                    GroupServiceOverseerPrivilage = GroupServiceOverseerPrivilage.GroupServiceDeputyOverseer,
                    MobileCartPrivilage = MobileCartPrivilage.LocalMobileCartPrivilage
                }
            });

            lst.Add(new Publisher()
            {
                IsDelete = false,
                CanLogin = false,
                Login = null,
                Password = null,
                Email = "tomasz.pomyslowy@gmail.com",
                Name = "Tomasz",
                Surname = "Pomysłowy",
                Congregation = cg,
                Sex = SexEnum.Male,
                PublisherGroup = dataForCongregation.PublisherGroup1,
                Privilage = new Privilage()
                {
                    PublisherPrivilage = PublisherPrivilage.BaptizedPublisher,
                    PionierPrivilage = PionierPrivilage.No,
                    CongregationPrivilage = CongregationPrivilage.No,
                    GroupServiceOverseerPrivilage = GroupServiceOverseerPrivilage.No,
                    MobileCartPrivilage = MobileCartPrivilage.LocalMobileCartPrivilage
                }
            });

            lst.Add(new Publisher()
            {
                IsDelete = false,
                CanLogin = false,
                Login = null,
                Password = null,
                Email = "monika.pomyslowy@gmail.com",
                Name = "Monika",
                Surname = "Pomysłowy",
                Congregation = cg,
                Sex = SexEnum.Female,
                PublisherGroup = dataForCongregation.PublisherGroup1,
                Privilage = new Privilage()
                {
                    PublisherPrivilage = PublisherPrivilage.BaptizedPublisher,
                    PionierPrivilage = PionierPrivilage.No,
                    CongregationPrivilage = CongregationPrivilage.No,
                    GroupServiceOverseerPrivilage = GroupServiceOverseerPrivilage.No,
                    MobileCartPrivilage = MobileCartPrivilage.LocalMobileCartPrivilage
                }
            });

            lst.Add(new Publisher()
            {
                IsDelete = false,
                CanLogin = false,
                Login = null,
                Password = null,
                Email = "juliusz.dzielony@gmail.com",
                Name = "Juliusz",
                Surname = "Dzielny",
                Congregation = cg,
                Sex = SexEnum.Male,
                PublisherGroup = dataForCongregation.PublisherGroup1,
                Privilage = new Privilage()
                {
                    PublisherPrivilage = PublisherPrivilage.BaptizedPublisher,
                    PionierPrivilage = PionierPrivilage.No,
                    CongregationPrivilage = CongregationPrivilage.No,
                    GroupServiceOverseerPrivilage = GroupServiceOverseerPrivilage.No,
                    MobileCartPrivilage = MobileCartPrivilage.LocalMobileCartPrivilage
                }
            });

            foreach (var p in lst)
            {
                DbContext.Add(p);
            }
            
        }

        private void GenerateTeritories(DataForCongregation dataForCongregation)
        {

            for (int i = 1; i <= 150; i++)
            {
                var teritoryCode = i.ToString().PadLeft(3, '0');
                var congregationCode = dataForCongregation.Congregation.Code;
                var t = new Teritory()
                {
                    Congregation = dataForCongregation.Congregation,
                    Code = $"{teritoryCode} [{congregationCode}]",
                    OrderView = i,
                    Name = $"Teritory {teritoryCode} [{congregationCode}]",
                };

                DbContext.Teritories.Add(t);
            }
        }
    }

    public class MockupDataHolder
    {
        public MockupDataHolder()
        {
            DataForCongregationA = new DataForCongregation();
            DataForCongregationB = new DataForCongregation();
        }

        public DataForCongregation DataForCongregationA { get; set; }
        public DataForCongregation DataForCongregationB { get; set; }
    }

    public class DataForCongregation
    {
        public Congregation Congregation { get; set; }

        public PublisherGroup PublisherGroup1 { get; set; }
        public PublisherGroup PublisherGroup2 { get; set; }
        public PublisherGroup PublisherGroup3 { get; set; }
        public PublisherGroup PublisherGroup4 { get; set; }
        public PublisherGroup PublisherGroup5 { get; set; }
    }


    public class MockupConsta_CongregationA
    {
        public const string CongregationCode = "CG-A";

        public const string PublisherGroup1 = "G1 [" + CongregationCode + "]";
        public const string PublisherGroup2 = "G2 [" + CongregationCode + "]";
        public const string PublisherGroup3 = "G3 [" + CongregationCode + "]";
        public const string PublisherGroup4 = "G4 [" + CongregationCode + "]";
        public const string PublisherGroup5 = "G5 [" + CongregationCode + "]";
    }

    public class MockupConsta_CongregationB
    {
        public const string CongregationCode = "CG-B";
    }

}
