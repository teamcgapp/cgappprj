﻿using System.Collections.Generic;
using System.Text;

namespace CGApp.Data
{

    public interface IDatabaseStructureManager
    {
        void ForceToCreateNewDatabase(string fileDbName);
    }
}
