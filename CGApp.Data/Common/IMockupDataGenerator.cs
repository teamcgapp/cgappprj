﻿namespace CGApp.Data
{
    public interface IMockupDataGenerator
    {
        void Generate();
    }
}
