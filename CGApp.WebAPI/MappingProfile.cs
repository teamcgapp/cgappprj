﻿using AutoMapper;
using CGApp.Data;
using CGApp.WebAPI.RemoteModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace CGApp.WebAPI
{

    public class MappingProfile: Profile
    {
        public MappingProfile()
        {            
            CreateMap<Teritory, RMTeritory>();
            CreateMap<RMTeritory, Teritory>();
            
            CreateMap<Publisher, RMPublisher>();
            CreateMap<RMPublisher,Publisher>();

            CreateMap<RMPrivilage, Privilage>();
            CreateMap<Privilage, RMPrivilage>();

            CreateMap<RMPublisherGroup, PublisherGroup>();
            CreateMap<PublisherGroup,RMPublisherGroup>();
        }
    }
}
