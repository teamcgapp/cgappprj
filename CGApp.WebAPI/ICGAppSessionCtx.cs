﻿using Microsoft.AspNetCore.Http;

namespace CGApp.WebAPI
{
    public interface ICGAppSessionCtx
    {
        int UserId { get; set; }
        int CongregationId { get; set; }

        void Invalidate();
    }

    public interface ICGAppSessionCtxSessionAccessor
    {
        ISession Session { get; set; }
    }


    public static class CGAppSessionCtxExtension
    {
        public static void SetHttpSession(this ICGAppSessionCtx ctx, ISession session)
        {
            if (ctx is ICGAppSessionCtxSessionAccessor)
            {
                (ctx as ICGAppSessionCtxSessionAccessor).Session = session;
            }
        }
    }
}
