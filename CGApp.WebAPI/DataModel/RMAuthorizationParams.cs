﻿namespace CGApp.WebAPI.RemoteModel
{
    public class RMAuthorizationParams
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
