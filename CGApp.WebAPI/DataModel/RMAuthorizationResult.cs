﻿namespace CGApp.WebAPI.RemoteModel
{
    public class RMAuthorizationResult
    {
        public bool AuthorizationStatus { get; set; }
    }
}
