﻿using System;
using System.Runtime.Serialization;

namespace CGApp.WebAPI.RemoteModel
{
    public class RMPublisher
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public bool IsDelete { get; set; }
        public bool IsOnlyLogin { get; set; }
        public bool CanLogin { get; set; }
        //public bool CanLoginToPublisherPanel { get; set; }
        public string Name { get; set; }

        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime DateOfBaptism { get; set; }
        public RMSexEnum Sex { get; set; }
        //SexDesc: string;
        [DataMember]
        public RMPrivilage Privilage { get; set; }
        public RMPublisherGroup PublisherGroup { get; set; }

    }
}



