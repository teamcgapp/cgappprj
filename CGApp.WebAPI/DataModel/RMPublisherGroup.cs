﻿namespace CGApp.WebAPI.RemoteModel
{
    public class RMPublisherGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Pos { get; set; }
    }
}



