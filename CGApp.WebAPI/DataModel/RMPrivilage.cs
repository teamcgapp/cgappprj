﻿namespace CGApp.WebAPI.RemoteModel
{
    public class RMPrivilage
    {
        public PublisherPrivilage PublisherPrivilage { get; set; }
        public PionierPrivilage PionierPrivilage { get; set; }
        public CongregationPrivilage CongregationPrivilage { get; set; }
        public GroupServiceOverseerPrivilage GroupServiceOverseerPrivilage { get; set; }
        public MobileCartPrivilage MobileCartPrivilage { get; set; }

    }

    public enum PublisherPrivilage { No = 0, UnbaptizedPublisher = 10, BaptizedPublisher = 20 }
    public enum PionierPrivilage { No = 0, AuxiliaryPionee = 10, RegularPioneer = 20, SpecialPioneer = 30 }
    public enum CongregationPrivilage { No = 0, MinisterialServant = 10, Elder = 20 }
    public enum GroupServiceOverseerPrivilage { No = 0, GroupServiceDeputyOverseer = 10, GroupServiceOverseer = 20 }
    public enum MobileCartPrivilage { No = 0, LocalMobileCartPrivilage = 10 }

}



