﻿using CGApp.Data;
using CGApp.WebAPI.RemoteModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;


namespace CGApp.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class AuthorizationController : Controller
    {
        private CGAppDbContext DbContext;
        private ICGAppSessionCtx SessionCtx;
        public AuthorizationController(CGAppDbContext dbContext, ICGAppSessionCtx sessionCtx)
        {
            DbContext = dbContext;
            SessionCtx = sessionCtx;
            //SessionCtx.SetHttpSession(HttpContext.Session);
        }

        [HttpPost("AuthorizationUser")]        
        public RMAuthorizationResult AutorizeUser(RMAuthorizationParams authorizationParams)
        {            
            var lg = new LoginHistory();
            Publisher publisher = null;
            
            lg.IsOk = false;
            lg.LoginTime = DateTime.Now;
            try
            {

                if (string.IsNullOrEmpty(authorizationParams.Login) ||
                    string.IsNullOrEmpty(authorizationParams.Password))
                {
                    lg.ParamsWasIncoret = true;
                    return new RMAuthorizationResult() { AuthorizationStatus = false };
                }

                lg.Login = authorizationParams.Login;

                var publisherList = (from pb in DbContext.Publishers.Include( x => x.Congregation)
                         where pb.Login == authorizationParams.Login
                         select pb).ToList();

                if (publisherList.Count == 0)
                {
                    lg.LoginWasNotFound = true;
                    return new RMAuthorizationResult() { AuthorizationStatus = false };
                }

                if (publisherList.Count > 1)
                {
                    lg.IsToMuchLogins = true;
                    return new RMAuthorizationResult() { AuthorizationStatus = false };
                }


                publisher = publisherList[0];
                lg.Publisher = publisher;
                if (!publisher.CanLogin)
                {
                    lg.IsCanLoginWasNotSet = true;
                    return new RMAuthorizationResult() { AuthorizationStatus = false };
                }
                if (authorizationParams.Password != publisher.Password)
                {
                    lg.PasswordWasWrong = true;
                    return new RMAuthorizationResult() { AuthorizationStatus = false };
                }

                if (publisher.Congregation == null)
                {
                    lg.CongregationWasNotSet = true;
                    return new RMAuthorizationResult() { AuthorizationStatus = false };
                }

                SessionCtx.CongregationId = publisher.Congregation.Id;
                SessionCtx.UserId = publisher.Id;

                lg.IsOk = true;

                return new RMAuthorizationResult() { AuthorizationStatus = true };
            }
            catch(Exception err)
            {
               lg.ExceptionMsg = err.Message;
                lg.IsWasException = true;
                return new RMAuthorizationResult() { AuthorizationStatus = false };
            }
            finally
            {
                IDbContextTransaction t = null;
                try
                {
                    //t = DbContext.Database.BeginTransaction();

                    
                    DbContext.Add(lg);
                    DbContext.SaveChanges();
                    //t.Commit();
                }
                catch(Exception err)
                {
                    //t.Rollback();
                    throw; //should go only to text log!!!
                }
            } 
        }

        [HttpPost("Logout")]
        public bool Logout()
        {
            SessionCtx.Invalidate();
            return true;
        }
    }
}
