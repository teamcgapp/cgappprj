﻿using AutoMapper;
using CGApp.Data;
using CGApp.WebAPI.RemoteModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CGApp.WebAPI.Controllers
{   

    [Route("api/[controller]")]
    public class TeritoryController : Controller
    {
        private CGAppDbContext DbContext;
        private IMapper Mapper;
        private ICGAppSessionCtx SessionCtx;
        public TeritoryController(CGAppDbContext dbContext, IMapper mapper, ICGAppSessionCtx sessionCtx)
        {
            DbContext = dbContext;
            Mapper = mapper;
            SessionCtx = sessionCtx;
            //SessionCtx.SetHttpSession(HttpContext.Session);
        }

        [HttpGet]
        public IEnumerable<RMTeritory> Get()
        {
            SessionCtx.ThrowExceptionIfCongregationIdWasNotSet();

            var q = from t in DbContext.Teritories
                    where t.Congregation.Id == SessionCtx.CongregationId
                    select t;

            var q2 = q.Select((item) => Mapper.Map<Teritory, RMTeritory>(item));

            return q2.ToAsyncEnumerable().ToEnumerable();
        }

        
        [HttpGet("{id}")]
        public RMTeritory Get(int id)
        {
            return new RMTeritory() { Name = "name1" };
        }
        
        [HttpPost]
        public RMTeritory Post([FromBody]RMTeritory teritory)
        {
            return null;
        }
        
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]RMTeritory teritory)
        {
        }
        
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    
}

