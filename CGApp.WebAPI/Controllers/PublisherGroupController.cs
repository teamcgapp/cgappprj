﻿using AutoMapper;
using CGApp.Data;
using CGApp.WebAPI.RemoteModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace CGApp.WebAPI
{
    [Route("api/[controller]")]
    public class PublisherGroupController : Controller
    {
        private CGAppDbContext DbContext;
        private IMapper Mapper;
        private ICGAppSessionCtx SessionCtx;
        public PublisherGroupController(CGAppDbContext dbContext, IMapper mapper, ICGAppSessionCtx sessionCtx)
        {
            DbContext = dbContext;
            Mapper = mapper;
            SessionCtx = sessionCtx;
            //SessionCtx.SetHttpSession(HttpContext.Session);
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<RMPublisherGroup> Get()
        {
            SessionCtx.ThrowExceptionIfCongregationIdWasNotSet();

            var q = from t in DbContext.PublisherGroups
                    where t.Congregation.Id == SessionCtx.CongregationId
                    select t;
            var q2 = q.Select((item) => Mapper.Map<PublisherGroup, RMPublisherGroup>(item));
            return q2.ToAsyncEnumerable().ToEnumerable();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public RMPublisherGroup Get(int id)
        {
            return new RMPublisherGroup() { Name = "name1" };
        }

        // POST api/values
        [HttpPost]
        public RMPublisherGroup Post([FromBody]RMPublisherGroup publisherGroup)
        {
            return null;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]RMPublisherGroup publisherGroup)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
