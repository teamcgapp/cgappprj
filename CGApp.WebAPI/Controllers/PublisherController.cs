﻿using AutoMapper;
using CGApp.Data;
using CGApp.WebAPI.RemoteModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace CGApp.WebAPI
{

    [Route("api/[controller]")]
    public class PublisherController : Controller
    {
        private CGAppDbContext DbContext;
        private IMapper Mapper;
        private ICGAppSessionCtx SessionCtx;
        public PublisherController(CGAppDbContext dbContext, IMapper mapper, ICGAppSessionCtx sessionCtx)
        {
            DbContext = dbContext;
            Mapper = mapper;
            SessionCtx = sessionCtx;
            //SessionCtx.SetHttpSession(HttpContext.Session);
        }
        // GET api/values
        [HttpGet]
        public IEnumerable<RMPublisher> Get()
        {
            SessionCtx.ThrowExceptionIfCongregationIdWasNotSet();

            var q = from t in DbContext.Publishers.
                Include(p => p.Privilage).
                Include(p => p.Congregation).
                Include(p => p.PublisherGroup)
                    where t.Congregation.Id == SessionCtx.CongregationId
                    select t;

            var q2 = q.Select((item) => Mapper.Map<Publisher, RMPublisher>(item));

            //return q2.ToAsyncEnumerable().ToEnumerable();
            var lst = q2.ToList();

            return lst.ToAsyncEnumerable().ToEnumerable();

        }

        // GET api/values/5
        [HttpGet("{id}")]
        public RMPublisher Get(int id)
        {
            return new RMPublisher() { Name = "name1" };
        }

        // POST api/values
        [HttpPost]
        public RMPublisher Post([FromBody]RMPublisher publisher)
        {
            return publisher;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]RMPublisher publisher)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    public static class ControllersEx
    {
        public static void ThrowExceptionIfCongregationIdWasNotSet(this ICGAppSessionCtx sessionCtx)
        {
            if (sessionCtx.CongregationId == 0)
            {
                throw new Exception("[SessionData] Congregation was not set");
            }
        }
    }
}
